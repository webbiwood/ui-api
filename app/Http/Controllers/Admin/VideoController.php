<?php

namespace App\Http\Controllers\Admin;

use App\Video;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class VideoController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin_auth:admin');
    }

    public function index()
    {
        $cards = Video::all();
        return view('admin.video.index', compact('cards'));
    }

    public function create()
    {
//        return view('admin.video.create',  compact('cards'));
    }
}
