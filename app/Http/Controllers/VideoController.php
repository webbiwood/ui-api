<?php

namespace App\Http\Controllers;

use App\Video;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;

class VideoController extends Controller
{
    public function index()
    {
        $video = Video::all();
        return $video;
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'file' => 'required|mimes:video/x-ms-asf,video/mp4,mkv,video/x-matroska,application/x-mpegURL,video/MP2T,video/quicktime,video/x-msvideo,video/x-ms-wmv,video/avi|max:20000',
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }
        //$data = $request->only(['file']);
        if ($request->file('file')) {

            $file = $request->file('file');
            $filename = time() . $file->getClientOriginalName();
            $path = public_path() . '/uploads/';
            $file->move(public_path() . '/uploads/', $filename);

            //Database
            //$video = Video::create($file);
        }

        return response()->json(['success' => 'You have successfully uploaded an image'], 200);
    }

    public function show($id)
    {
        return Video::findOrFail($id);
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        $video = Video::findOrFail($id);
        $video->update($request->all());

        return $video;
    }

    public function destroy($id)
    {
        $video = Video::findOrFail($id);
        $video->delete();
        return response(null, Response::HTTP_OK);
    }
}
