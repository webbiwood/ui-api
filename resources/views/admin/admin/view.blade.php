@extends('admin.layouts.app')
@section('content')
    <!-- Bread crumb -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-primary">Admin Details</h3> </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Home</a></li>
                <li class="breadcrumb-item active"><a href="{{ route('admin.list') }}">Admin</a></li>
                <li class="breadcrumb-item active"> Details</li>
            </ol>
        </div>
    </div>
    <!-- End Bread crumb -->
<div class="container-fluid">
    <!-- Start Page Content -->
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <h3 class="text-primary pull-left">@if(isset($user->id)){{ $user->name }} Details @endif</h3>
                    <span class="nav pull-right panel_toolbox">
                            <a class="btn btn-success" href="{{ route('admin.list') }}">
                                <i class="fa fa-plus-circle"></i>Back
                            </a>
                        </span>
                </div>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="card">
                <div class="card-title">
                    <h4>Profile</h4>
                </div>
                <div class="sales-chart">
                    <div class="profile_img">
                        <div id="crop-avatar">
                            <!-- Current avatar -->
                            @if(isset($user))
                                @if($user->id)
                                    <div class="text-center">
                                        @if(isset($user->avatar))
                                            @if($user->avatar == 'images/user.png')
                                                <img src="{{ asset('/') }}images/img.jpg" alt="avatar" class="img-circle profile_img">
                                            @else
                                                <a href="{{ asset('storage/admin/avatar/'. $user['id'] . '/' .$user['avatar']) }}" class="text-center"><img src="{{ asset('storage/admin/avatar/'. $user['id'] . '/' .$user['avatar']) }}" name="aboutme" width="140" height="140" border="0" class="img-circle"></a>
                                            @endif
                                        @else
                                            <img src="{{ asset('/') }}images/img.jpg" alt="avatar" class="img-circle profile_img">
                                        @endif
                                    </div>
                                @endif
                            @endif
                        </div>
                    </div>

                    <ul class="list-unstyled user_data">

                        <li>
                            <i class="fa fa-briefcase user-profile-icon"></i> {{ $user->post }}
                        </li>
                        <li>
                            <i class="fa fa-envelope user-profile-icon"></i> {{ $user->email }}
                        </li>
                    </ul>

                    <a href="{{ route('admin.edit', ['id'=> $user['id']]) }}" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i> Edit </a>
                    <a href="{{ route('admin.delete', ['id'=> $user['id']]) }}" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> Delete </a>
                    <br />
                </div>
            </div>
            <!-- /# card -->
        </div>
        <!-- /# column -->
    </div>
    <!-- /# row -->
    <!-- End PAge Content -->
</div>
@endsection