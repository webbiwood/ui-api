<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new \App\User();
        $user->user = 'chris';
        $user->name = 'Chris Surulere';
        $user->email = 'suruabiye@gmail.com';
        $user->password = bcrypt('chris1234@@');
        $user->mobile = '030566178';
        $user->gender = 'Male';
        $user->is_admin = 1;
        $user->status = 'active';
        $user->canStream = 1;
        $user->canUpload = 1;
        $user->lastLogin = Carbon\Carbon::now();
        $user->remember_token = str_random(60);
        $user->emailVerified = 1;
        $user->created_at = Carbon\Carbon::now();
        $user->updated_at = Carbon\Carbon::now();
        $user->save();
    }
}
