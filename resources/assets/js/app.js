import {setAuthorization} from "./helpers/general";

require('./bootstrap');
import Vue from 'vue'
import VueRouter from 'vue-router';
import App from './MainApp';
import router from './routes';
import Default from './layouts/Default';
import Auth from './layouts/Auth'
import store from './store';
import VeeValidate from 'vee-validate';


Vue.use(VueRouter);
Vue.use(VeeValidate);

VeeValidate.Validator.extend('verify_email', {
    getMessage: field => 'Email Already exists',
    validate: value => new Promise((res, rej) => {
        axios.post('/api/user/email',  { email: value })
            .then((response) => {
                res(!!response.data.valid);
            })
            .catch(function (error) {
                rej(error);
            });
    })
});

Vue.component('app-layout', Default);
Vue.component('authLR-layout', Auth);

Vue.component(
    'passport-clients',
    require('./components/passport/Clients.vue')
);

Vue.component(
    'passport-authorized-clients',
    require('./components/passport/AuthorizedClients.vue')
);

Vue.component(
    'passport-personal-access-tokens',
    require('./components/passport/PersonalAccessTokens.vue')
);

Vue.config.productionTip = false;

const app = new Vue({
    router,
    store,
    render: h => h(App),
    el: '#page-top'
});

import 'vue2-dropzone/dist/vue2Dropzone.css'
