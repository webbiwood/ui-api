import VueRouter from "vue-router";

const Home = resolve => require(['./pages/Home'], resolve);
const Channels = resolve => require(['./pages/ChannelPages/Index'], resolve);
const SingleChannel = resolve => require(['./pages/ChannelPages/Single'], resolve);
const Playlist = resolve => require(['./pages/Playlist'], resolve);
const Videos = resolve => require(['./pages/Videos'], resolve);
const Upload = resolve => require(['./pages/Upload'], resolve);
const UploadVideo = resolve => require(['./pages/UploadVideo'], resolve);
const Categories = resolve => require(['./pages/Categories'], resolve);
const Subscriptions = resolve => require(['./pages/Subscriptions'], resolve);
const Settings = resolve => require(['./pages/Settings'], resolve);
const Account = resolve => require(['./pages/Account'], resolve);
const Register = resolve => require(['./pages/Auth/Register'], resolve);
const Login = resolve => require(['./pages/Auth/Login'], resolve);
const ForgotPassword = resolve => require(['./pages/Auth/ForgotPassword'], resolve);

// handling 404
const NotFound = resolve => require(['./components/NotFound'], resolve);

// const Messages = resolve => require(['./pages/Messages'], resolve);
// const ProfileBase = resolve => require(['./pages/profile/Profile'], resolve);
// const Overview = resolve => require(['./pages/profile/Overview'], resolve);
// const ForgotPassword = resolve => require(['./pages/ForgotPassword'], resolve);
// const ResetPassword = resolve => require(['./pages/ResetPassword'], resolve);

export const routes = [{
        path: '/',
        name: 'index',
        component: Home
    },
    {
        path: '/channels',
        name: 'channels',
        component: Channels
    },
    {
        path: '/single-channel',
        name: 'single-channel',
        component: SingleChannel
    },
    {
        path: '/playlist',
        name: 'playlist',
        component: Playlist
    },
    {
        path: '/videos',
        name: 'videos',
        component: Videos
    },
    {
        path: '/user/upload-video',
        name: 'upload',
        component: Upload
    },
    {
        path: '/user/upload-video-details',
        name: 'upload-details',
        component: UploadVideo
    },
    {
        path: '/categories',
        name: 'categories',
        component: Categories
    },

    // Everything About user here
    {
        path: '/register',
        name: 'register',
        component: Register,
        meta: {
            layout: "authLR"
        }
    },
    {
        path: '/login',
        name: 'login',
        component: Login,
        meta: {
            layout: "authLR"
        }
    }, // For the Guest
    {
        path: '/forgot-password',
        name: 'forgotpassword',
        component: ForgotPassword,
        meta: {
            layout: "authLR"
        }
    },
    {
        path: '/user/account',
        name: 'account',
        component: Account
    },
    {
        path: '/user/subscriptions',
        name: 'subscriptions',
        component: Subscriptions
    },
    {
        path: '/user/settings',
        name: 'settings',
        component: Settings
    },

    // To handle 404 page..
    {
        path: '/404',
        component: NotFound
    },
];

export default new VueRouter({
    hashbang: false,
    history: true,
    mode: 'history',
    routes,
});