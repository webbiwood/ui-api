<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVideosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('videos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('clean_title');
            $table->text('description')->nullable();
            $table->integer('views_count');
            $table->enum('status',['a','i','e','x','d','xmp4','xwebm','xmp3','xogg','ximg','u','p'])->default('e')
                ->comment('a = active,i = inactive, e = encoding, x = encoding error, d = downloading, u = Unlisted,
                p = private, xmp4 = encoding mp4 error, xwebm = encoding webm error, xmp3 = encoding mp3 error, xogg = encoding ogg error,
                 ximg = get image error');
            $table->integer('user_id');
            $table->integer('category_id');
            $table->string('filename');
            $table->string('duration');
            $table->enum('type', ['audio', 'video', 'embed']);
            $table->string('videoDownloadedLink');
            $table->integer('order')->default(1);
            $table->smallInteger('rotation')->nullable()->default(0);
            $table->float('zoom')->nullable()->default(1);
            $table->string('youtubeId')->nullable();
            $table->string('videoLink')->nullable();
            $table->integer('next_videos_id')->nullable();
            $table->integer('isSuggested')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('videos');
    }
}
