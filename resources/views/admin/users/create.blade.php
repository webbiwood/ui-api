@extends('admin.layouts.app')
@section('content')
<div class="container-fluid">
    <!-- Bread crumb -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-primary">@if(isset($edit)) Edit @else New @endif User</h3> </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Home</a></li>
                <li class="breadcrumb-item active"><a href="{{ route('admin.user.list') }}">User</a></li>
                <li class="breadcrumb-item active"> @if(isset($edit)) Edit @else New @endif User</li>
            </ol>
        </div>
    </div>
    <!-- End Bread crumb -->
    <!-- Start Page Content -->
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <h5 class="text-primary pull-left">@if(isset($dataTypeContent->id)){{ 'Edit' }}@else{{ 'Create New' }} User @endif</h5>
                    <span class="nav pull-right panel_toolbox">
                            <a class="btn btn-success" href="{{ route('admin.user.list') }}">
                                <i class="fa fa-plus-circle"></i>Back
                            </a>
                        </span>
                </div>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="card">
                <div class="card-body">
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    @if(Session::has('message'))
                        <div class="alert alert-success">
                            {{ Session::get('message') }}
                        </div>
                    @endif
                    <div class="basic-form">
                        <form class="form-edit-add" role="form"
                              action="@if(isset($dataTypeContent->id)){{ route('admin.user.edit', $dataTypeContent->id) }}@else{{ route('admin.user.create') }}@endif"
                              method="POST" enctype="multipart/form-data">
                            <!-- CSRF TOKEN -->
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for="name">Name</label>
                                <input type="text" class="form-control input-flat" name="name"
                                       placeholder="Name" id="name"
                                       value=" @if(isset($dataTypeContent->id)){{ old('name', $dataTypeContent->name) }} @else {{ old('name') }} @endif ">
                            </div>
                            <div class="form-group">
                                <label for="name">Username</label>
                                <input type="text" class="form-control input-flat" name="username"
                                       placeholder="Username"
                                       value="@if(isset($dataTypeContent->id)){{ old('username', $dataTypeContent->username) }} @else {{ old('username') }}@endif">
                            </div>
                            <div class="form-group">
                                <label for="name">Email</label>
                                <input type="text" class="form-control input-flat" name="email"
                                       placeholder="Email" id="email"
                                       value="@if(isset($dataTypeContent->id)){{ old('email', $dataTypeContent->email) }} @else {{ old('email') }}@endif">
                            </div>

                            <div class="form-group">
                                <label for="name">Phone</label>
                                <input type="text" class="form-control input-flat" name="phone"
                                       placeholder="Phone" id="phone"
                                       value="@if(isset($dataTypeContent->id)){{ old('phone', $dataTypeContent->phone) }} @else {{ old('phone') }} @endif">
                            </div>

                            <div class="form-group">
                                <label for="password">Password @if(isset($dataTypeContent->id)) (Leave empty if you don't want to change password) @endif </label>
                                <input type="password" class="form-control input-flat" name="password"
                                       placeholder="Password" id="password"
                                       value="{{old('password')}}">
                            </div>
                            <div class="form-group">
                                <label for="name">Address</label>
                                <input type="text" class="form-control input-flat" name="address"
                                       placeholder="Address" id="phone"
                                       value="@if(isset($dataTypeContent->id)){{ old('address', $dataTypeContent->address) }} @else {{ old('address') }} @endif ">
                            </div>
                            <div class="form-group">
                                <label for="avatar">Photo</label>
                                <div>
                                    Select an image(leave empty if you don't want to change your photo)
                                    <input type="file" name="avatar" class="form-control input-flat">
                                </div>
                            </div>
                            <div class="panel-footer">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- /# column -->
    </div>
    <!-- /# row -->

    <!-- End PAge Content -->
</div>
@endsection
@section('js')

@stop
