<?php

namespace App\Http\Controllers;


use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    public function __construct()
    {
        //$this->middleware('auth:api', ['except' => ['login']]);
    }

    public function test()
    {
        return response()->json(User::all());
    }
    public function index()
    {
        return response()->json(User::all());
    }

    public function emailCheck(Request $request)
    {
        $user = User::where('email', $request->get('email'))->exists(); // or whatever else way you would check.

        if(!$user) {
            // return TRUE when the user doesn't exists, so that one can proceed
            return response()->json([
                'valid' => true
            ]);
        }

        // return FALSE when the user exists, so that one can't proceed
        return response()->json([
            'error' => false
        ]);
    }

    public function login(Request $request)
    {
        $status = 401;
        $response = ['error' => 'Unauthorised'];

        if (Auth::attempt($request->only(['email', 'password']))) {
            $status = 200;
            $response = [
                'user' => Auth::user(),
                'token' => Auth::user()->createToken('webbiWood')->accessToken,
            ];
        }

        return response()->json($response, $status);
    }

    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'user' => 'required',
            'name' => 'required|max:50',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:4',
            'mobile' => 'required',
            'gender' => 'required',
            //'confpassword' => 'required|same:password',
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }

        $data = $request->only(['user','name', 'email', 'password', 'mobile','gender']);
        $data['password'] = bcrypt($data['password']);

        $user = User::create($data);
        $user->is_admin = 0;

        return response()->json([
            'user' => $user,
            'token' => $user->createToken('webbiWood')->accessToken,
        ]);
    }

    public function show($id)
    {
        return response()->json(User::findOrFail($id));
    }

    public function update(Request $request, $id)
    {
        $user = User::findOrFail($id);
        $user->update($request->all());

        return $user;
    }

    public function destroy($id)
    {
        $user = User::findOrFail($id);
        $user->delete();
        return response(null, Response::HTTP_OK);
    }
}
