<?php

namespace App\Http\Controllers\Admin;

use App\Hire;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HireController extends Controller
{
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth:admin');
	}
	public function application()
	{
		$applications = Hire::all();
		return view('admin.hire.applications', compact('applications'));
	}
}
