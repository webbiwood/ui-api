export default (api) => ({
    namespaced: true,
    state: {
        playlist: {},
        playlistCollection: [],
    },
    mutations: {
        setPlaylist: (state, data) => state.playlist = data,
        setPlaylistCollection: (state, data) => state.playlistCollection = data
    },
    actions: {
        getPlaylistCollection: ({commit, dispatch}, params) =>
            api.list('playlist-collection', params)
                .then((response) => {
                    commit('setPlaylistCollection', response);
                })
                .catch((err) => console.log(err)),
        getPlaylist: ({commit, dispatch}, id) => api.get('playlist', id)
            .then((response) => {
                commit('setPlaylist', response);
            })
            .catch((err) => console.log(err))
    },
    getters: {
        playlist: state => state.playlist,
        playlistCollection: state => state.playlistCollection
    }
})
