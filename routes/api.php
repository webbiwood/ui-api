<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::post('login', 'UserController@login');
Route::post('register', 'UserController@register');
Route::post('user/email', 'UserController@emailCheck');

Route::group(['as' => 'api.'], function () {

    /**
     * Users Endpoint
     */
    Route::resource('users', 'UserController', ['except' => ['create', 'edit']]);

    /**
     * Videos Endpoint
     */
    Route::resource('videos', 'VideoController', ['except' => ['create', 'edit']]);

    /**
     * Videos Endpoint
     */
    //Route::get('videos', 'VideoController@index');
    //Route::get('video/{video_id}', 'VideoController@show');
    //Route::put('video/{video_id}', 'VideoController@update');
    //Route::post('videos', 'VideoController@store');
    //Route::delete('video/{video_id}', 'VideoController@destroy');


    /**
     * Channels
     */
    //Route::get('channels', 'ChannelController@index'); // All channels in webbiwood.
    //Route::post('channels', 'ChannelController@store');
    //Route::get('channels/{user_id}', 'ChannelController@index'); // All channels by a user.
    //Route::get('channel/{channel_id}', 'ChannelController@show');
    //Route::put('channel/{channel_id}', 'ChannelController@update');
    //Route::delete('channel/{channel_id}', 'ChannelController@destroy');
});
