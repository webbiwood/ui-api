<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'SpaController@index');


/*///////////////////
 * Admin routes ////
*//////////////////
Route::group(['prefix' => 'admin', 'namespace' => 'Admin'], function () {

    //Handle admin login and password reset here....
    Route::group(['middleware' => ['admin_guest']], function(){
        Route::get('/login', 'LoginController@showLoginForm')->name('admin.login');
        Route::post('/login', 'LoginController@login')->name('admin.login.post');
        //Route::post('/reset', 'LoginController@login')->name('admin.login.post');
    });

    Route::group(['middleware' => 'admin_auth'], function(){

        //Start your life here...
        Route::get('/', 'DashBoardController@index')->name('admin.dashboard');
        Route::get('/dashboard', 'DashBoardController@index')->name('admin.dashboard');//Dashboard routes
        /*
         * Handling Users and Admin
         */
        // Admin
        Route::get('/list', 'AdminController@index')->name('admin.list');
        Route::get('/list/{id}/view', 'AdminController@show')->name('admin.view');
        Route::get('/profile/me', 'AdminController@profile')->name('admin.self.profile');
        Route::get('/create', 'AdminController@create')->name('admin.create');
        Route::post('/create', 'AdminController@store')->name('admin.create');
        Route::get('/{id}/edit', 'AdminController@edit')->name('admin.edit');
        Route::post('/{id}/edit', 'AdminController@update')->name('admin.edit');
        Route::get('/{id}/delete', 'AdminController@destroy')->name('admin.delete');
        Route::post('/logout', 'LoginController@logout')->name('admin.logout');
        //users
        Route::get('/users/list', 'UserController@index')->name('admin.user.list');
        Route::get('/user/list/{id}/view', 'UserController@show')->name('admin.user.view');
        Route::get('/user/create', 'UserController@create')->name('admin.user.create');
        Route::post('/user/create', 'UserController@store')->name('admin.user.create');
        Route::get('/user/{id}/edit', 'UserController@edit')->name('admin.user.edit');
        Route::post('/user/{id}/edit', 'UserController@update')->name('admin.user.edit');
        Route::get('/user/{id}/delete', 'UserController@destroy')->name('admin.user.delete');
        //playlist
        Route::get('/playlist', 'PlaylistController@playlist')->name('admin.playlist');
        //Route::get('/playlist/{id}', 'PlaylistController@playlist')->('admin.playlist');

        /*
		 * Handling Videos
		 */
        Route::get('/video/list', 'VideoController@index')->name('admin.video');
        Route::get('/video/{slug}/view', 'VideoController@index')->name('admin.video.view');
        Route::get('/video/details/{slug}','VideoController@show')->name('admin.video.details');
        Route::get('/video/create','VideoController@create')->name('admin.video.create');
        Route::post('/video/create','VideoController@store')->name('admin.video.create');
        Route::get('/video/edit/{slug}','VideoController@edit')->name('admin.video.edit');
        Route::post('/video/edit/{slug}','VideoController@update')->name('admin.video.edit');
        Route::get('/video/activate/{slug}','VideoController@activate')->name('admin.video.activate');
        Route::get('/video/deactivate/{slug}','VideoController@deactivate')->name('admin.video.deactivate');
        //Route::get('/video/delete/{slug}','VideoController@destroy')->name('admin.video.delete');

        //Category
        Route::get('/videos/category', 'CategoryController@index')->name('admin.video.category');
        Route::get('/videos/category/create', 'CategoryController@index')->name('admin.video.category.create');
        Route::post('/videos/category/create', 'CategoryController@store')->name('admin.video.category.create');
        Route::get('/videos/category/{id}/edit', 'CategoryController@edit')->name('admin.video.category.edit');
        Route::post('videos/category/{id}/update', 'CategoryController@update')->name('admin.video.category.update');
        Route::get('/videos/category/{id}/delete', 'CategoryController@destroy')->name('admin.video.category.delete');
    });
});

/**
 * Justice:: I put the {any} uri handler here so that it can catch all other pages that does not belong to the admin and foward it to the spa. ( Vue.js )
 */
Route::get('/{any}', 'SpaController@index')->where('any', '.*');
