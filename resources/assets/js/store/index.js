import Vue from 'vue';
import Vuex from "vuex";
import {getLocalUser} from "../helpers/auth";
import {API} from '../helpers';
import Playlist from './playlist';
import Users from './users';

Vue.use(Vuex);
const user = getLocalUser();

export default new Vuex.Store({
    state: {
        currentUser: user,
        isLoggedIn: !!user,
        isRegistered: !!user,
        loading: false,
        auth_error: null,
    },
    getters: {
        isLoading(state) {
            return state.currentUser;
        },
        isLoggedIn(state) {
            return state.isLoggedIn;
        },
        currentUser(state) {
            return state.currentUser;
        },
        authError(state) {
            return state.auth_error;
        }
    },
    mutations: {
        register(state) {
            state.loading = true;
            state.auth_error = null;
        },
        registerSuccess(state, payload) {
            state.auth_error = null;
            state.isRegistered = true;
            state.loading = false;
            state.currentUser = Object.assign({}, payload.user, {token: payload.access_token});

            localStorage.setItem("user", JSON.stringify(state.currentUser));
        },
        registerFailed(state, payload) {
            state.loading = false;
            state.auth_error = payload.error;
        },
        login(state) {
            state.loading = true;
            state.auth_error = null;
        },
        loginSuccess(state, payload) {
            state.auth_error = null;
            state.isLoggedIn = true;
            state.loading = false;
            state.currentUser = Object.assign({}, payload.user, {token: payload.access_token});

            localStorage.setItem("user", JSON.stringify(state.currentUser));
        },
        loginFailed(state, payload) {
            state.loading = false;
            state.auth_error = payload.error;
        },
        logout(state) {
            localStorage.removeItem("user");
            state.isLoggedIn = false;
            state.currentUser = null;
        },
    },
    actions: {
        login(context) {
            context.commit("login");
        },
        register(context) {
            context.commit("register");
        },
    },
    modules: {
        playlistStore: Playlist(API),
        userStore: Users(API),
        // videoStore: Videos(API)
    }
});
