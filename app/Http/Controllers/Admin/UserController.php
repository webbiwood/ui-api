<?php

namespace App\Http\Controllers\Admin;

use Response;
use Validator;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('admin_auth:admin');
    }
    public function index()
    {
        $users = User::all();
        return view('admin.users.users', compact('users'));
    }
    public static function getAllAdmins() {
        return User::all()->count();
    }
    public function create()
    {
        return view('admin.users.create');
    }
    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|unique:users',
            'username' => 'required|unique:users',
            'email' => 'required|email|unique:users',
            'phone' => 'required',
            'password' => 'required',
            'address' => 'required',
            'avatar' => 'required|image|max:10000',
        ];
        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()){
            return Redirect()->back()->withErrors($validator)->withInput();
        } else {
            // No validation error...

            //The user is a student
            // Insert data into database..
            $user = User::create([
                'name' => $request['name'],
                'username' => $request['username'],
                'email' => $request['email'],
                'phone' => $request['phone'],
                'password' => bcrypt($request['password']),
                'avatar' => $request['image'],
                'address' => $request['address'],
            ]);

            $lastId = $user->id;
            //Get last Developer slug
            $lastUser = User::findOrFail($lastId);

            /*
             * First make sure the avatar directory of the current user is ready by;
             * checking if its readable ie. if it available,
             * if not create the folder
             */
            // user avatar path..
            $path = public_path().'/storage/avatar/'.$lastId;

            try{
                //check if path exists
                if(!File::exists($path)) {
                    // path does not exist, create one..
                    File::makeDirectory($path, $mode = 0777, true, true);
                }
            } catch (Exception $exception ){
                $exception->getMessage();
            }
            if (empty($request->avatar)){
                // Leave empty...
                return Redirect()->back()->withErrors($validator)->withInput();
            }else{
                //Check for current avatar
                $avatar = $lastUser->avatar;
                $currentAvatar =  $path . '/'. $avatar; // Current avatar
                // Update the user profile table and set the profile picture
                //If profile pix already exist
                if (File::exists($currentAvatar)) {
                    $photo        = $request->avatar;
                    $fileNameExt  = $photo->getClientOriginalName(); // get filename eg 123.jpg
                    $filename_new = $path . '/' . $fileNameExt;
                    Image::make( $photo )->resize( 250, 205 )->save( $filename_new );

                    User::where('id', '=', $lastUser->id)->update([
                        'avatar' => $fileNameExt,
                    ]);
                }else{
                    //else if doesnt exist, try inserting it into database..
                    $photo = $request->avatar;
                    $fileNameExt = $photo->getClientOriginalName();
                    $filename_new = $path . '/' . $fileNameExt;
                    Image::make($photo)->resize(250, 205)->save($filename_new);
                    User::where('id', '=', $lastUser->id)->update([
                        'avatar' => $fileNameExt,
                    ]);
                }
            }
            return redirect()->route('admin.user.list')->with("message", "User Created");
        }
    }
    public function show($id)
    {
        $user = User::find($id);
        return view('admin.users.view', compact('user'));
    }
    public function showUser($id)
    {
        $user = User::find($id);
        return view('admin.users.view', compact('user'));
    }
    public function edit($id)
    {
        if($id != null){
            $dataTypeContent = User::find($id);
        }
        return view('admin.users.edit', compact('dataTypeContent'));
    }
	public function update(Request $request, $id)
	{
		$rules = [
            'name' => 'required',
            'username' => 'required',
            'email' => 'required|email',
            'phone' => 'required',
            //'password' => 'required',
            'address' => 'required',
            //'usertype' => 'required',
            //'avatar' => 'required|image|max:10000',
		];

		$validator = Validator::make($request->all(), $rules);

		if ($validator->fails()) {
			return Redirect()->back()->withErrors($validator)->withInput();
		} else {

			// No validation error...
			// Insert data into database..
            $user = [];
			if($request['password'] == ''){
				$user = User::where('id', $id)
				     ->update([
					     'name' => $request['name'],
					     'username' => $request['username'],
					     'email' => $request['email'],
                         'phone' => $request['phone'],
					     'avatar' => $request['avatar'],
                         'address' => $request['address'],
				     ]);
			}else{
				$user = User::where('id', $id)
				     ->update([
					     'name' => $request['name'],
					     'username' => $request['username'],
					     'email' => $request['email'],
					     'password' => bcrypt($request['password']),
                         'phone' => $request['phone'],
                         'avatar' => $request['avatar'],
                         'address' => $request['address'],
				     ]);
			}

            $lastId = $id;
            //Get last Developer slug
            $lastUser = User::findOrFail($lastId);

            /*
             * First make sure the avatar directory of the current user is ready by;
             * checking if its readable ie. if it available,
             * if not create the folder
             */
            // user avatar path..
            $path = public_path().'/storage/avatar/'.$lastId;

            try{
                //check if path exists
                if(!File::exists($path)) {
                    // path does not exist, create one..
                    File::makeDirectory($path, $mode = 0777, true, true);
                }
            } catch (Exception $exception ){
                $exception->getMessage();
            }
            if (empty($request->avatar)){
                // Leave empty...
                return Redirect()->back()->withErrors($validator)->withInput();
            }else{
                //Check for current avatar
                $avatar = $lastUser->avatar;
                $currentAvatar =  $path . '/'. $avatar; // Current avatar
                // Update the user profile table and set the profile picture
                //If profile pix already exist
                if (File::exists($currentAvatar)) {
                    $photo        = $request->avatar;
                    $fileNameExt  = $photo->getClientOriginalName(); // get filename eg 123.jpg
                    $filename_new = $path . '/' . $fileNameExt;
                    Image::make( $photo )->resize( 250, 205 )->save( $filename_new );

                    User::where('id', '=', $lastUser->id)->update([
                        'avatar' => $fileNameExt,
                    ]);
                }else{
                    //else if doesnt exist, try inserting it into database..
                    $photo = $request->avatar;
                    $fileNameExt = $photo->getClientOriginalName();
                    $filename_new = $path . '/' . $fileNameExt;
                    Image::make($photo)->resize(250, 205)->save($filename_new);
                    User::where('id', '=', $lastUser->id)->update([
                        'avatar' => $fileNameExt,
                    ]);
                }
            }
			return redirect()->route('admin.user.list')->with("message", "user Updated");
		}
	}
	public function suspend($id)
	{
		User::where('id', $id)->update([
			'status' =>'inactive',
			'suspended_at' => now()
		]);
		return redirect()->back()->with(['message'=>"User suspended"]);
	}
	public function unSuspend($id)
	{
		User::where('id', $id)->update([
			'status' =>'active',
			'unsuspended_at' => now(),
		]);
		return redirect()->back()->with(['message'=>"User is now active"]);
	}
    public function destroy($id)
    {
        return "You are not authorized to perform this operation";
    }
    public function viewResume($id)
    {
        return view('admin.resume.view', compact('id'));
    }
}
