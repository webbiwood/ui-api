<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin_auth:admin');
    }

    public function index() {
        $edit = '';
        $categories = Category::all();
        return view('admin.video.category', compact('categories', 'edit'));
    }

    public function store(Request $request) {
        $this->validate($request, [
            'name' => 'required|unique:card_categories|max:255',
            //'slug' => 'required|unique:developer_portfolio_categories|max:255',
        ]);
        Category::create([
            'name' => $request['name'],
            //'slug' => str_slug($request['slug']),
        ]);
        return redirect()->route('admin.video.category')->with("message", "Category Inserted");
    }

    public function show($id) {
        $edit = '';
        $categories = Category::all();
        return view('admin.video.category', compact('categories', 'edit'));
    }

    public function edit($id = "") {
        $edit = $id;
        $categories = Category::all();
        return view('admin.video.category', compact('categories', 'edit'));
    }

    public function update(Request $request, $id) {
        $rules = [
            'name' => 'required',
            //'slug' => 'required',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return Redirect()->back()->withErrors($validator);
        } else {
            // No validation error...
            // Insert data into database..
            Category::where('id', $id)
                ->update([
                    'name' => $request->name,
                    //'slug' => $request->slug,
                ]);
            return redirect()->route('admin.video.category')->with("message", "Category Updated");
        }
    }

    public function destroy($id) {
        $check = Category::find($id)->delete();
        //$check->delete();
        return redirect()->route('admin.video.category')->with("message", "Category Deleted");
    }
}
