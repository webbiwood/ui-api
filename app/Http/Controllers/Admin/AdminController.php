<?php

namespace App\Http\Controllers\Admin;

use App\User;
//use Intervention\Image\Image;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin_auth:admin');
    }
    public function index()
    {
        $users = User::where('is_admin','=',1)->get();
        return view('admin.admin.index', compact('users'));
        return view('admin.admin.index', compact('users'));
    }
    public function create()
    {
        return view('admin.admin.create');
    }
    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|unique:admins',
            'username' => 'required|unique:admins',
            'email' => 'required|email|unique:admins',
            'phone' => 'required',
            'password' => 'required',
            'avatar' => 'required|image|max:10000',
            'address' => 'required',
        ];
        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()){
            return Redirect()->back()->withErrors($validator)->withInput();
        } else {

            //The user is an admin
            // Insert data into database..
            $user  = User::create([
                'name' => $request['name'],
                'username' => $request['username'],
                'email' => $request['email'],
                'phone' => $request['phone'],
                'address' => $request['address'],
                'password' => bcrypt($request['password']),
                'is_admin' => 1,
                //'avatar' => $request['image'],
            ]);

            $lastId = $user->id;
            //Get last Admin slug
            $lastUser = User::findOrFail($lastId);

            /*
             * First make sure the avatar directory of the current user is ready by;
             * checking if its readable ie. if it available,
             * if not create the folder
             */
            // user avatar path..
            $path = public_path().'/storage/admin/avatar/'.$lastId;

            try{
                //check if path exists
                if(!File::exists($path)) {
                    // path does not exist, create one..
                    File::makeDirectory($path, $mode = 0777, true, true);
                }
            } catch (Exception $exception ){
                $exception->getMessage();
            }
            if (empty($request->avatar)){
                // Leave empty...
                return Redirect()->back()->withErrors($validator)->withInput();
            }else{
                //Check for current avatar
                $avatar = $lastUser->avatar;
                $currentAvatar =  $path . '/'. $avatar; // Current avatar
                // Update the user profile table and set the profile picture
                //If profile pix already exist
                if (File::exists($currentAvatar)) {
                    $photo        = $request->avatar;
                    $fileNameExt  = $photo->getClientOriginalName(); // get filename eg 123.jpg
                    $filename_new = $path . '/' . $fileNameExt;
                    Image::make( $photo )->resize( 250, 205 )->save( $filename_new );

                    User::where('id', '=', $lastUser->id)->update([
                        'avatar' => $fileNameExt,
                    ]);
                }else{
                    //else if doesnt exist, try inserting it into database..
                    $photo = $request->avatar;
                    $fileNameExt = $photo->getClientOriginalName();
                    $filename_new = $path . '/' . $fileNameExt;
                    Image::make($photo)->resize(250, 205)->save($filename_new);
                    User::where('id', '=', $lastUser->id)->update([
                        'avatar' => $fileNameExt,
                    ]);
                }
            }

            return redirect()->route('admin.list')->with("message", "Admin user created");

        }
    }
    public function show($id)
    {
        //echo public_path().'/storage/admin/avatar/';
        $user = User::find($id);
        return view('admin.admin.view', compact('user'));
    }
    public function edit($id)
    {
        if($id != null){
            $dataTypeContent = User::find($id);
        }
        return view('admin.admin.edit', compact('dataTypeContent'));
    }
    public function update(Request $request, $id)
    {
        $rules = [
            'name' => 'required',
            'username' => 'required',
            'email' => 'required|email',
            'phone' => 'required',
            'address' => 'required',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return Redirect()->back()->withErrors($validator)->withInput();
        } else {

            // No validation error...
            // Insert data into database..
            if($request['password'] == ''){
                $user = User::where('id', $id)
                    ->update([
                        'name' => $request['name'],
                        'username' => $request['username'],
                        'email' => $request['email'],
                        'phone' => $request['phone'],
                        'address' => $request['address'],
                    ]);
            }else{
                $user = User::where('id', $id)
                    ->update([
                        'name' => $request['name'],
                        'username' => $request['username'],
                        'email' => $request['email'],
                        'phone' => $request['phone'],
                        'password' => bcrypt($request['password']),
                        'address' => $request['address'],
                    ]);
            }

            $lastId = $id;
            //Get last Admin slug
            $lastUser = User::findOrFail($lastId);

            /*
             * First make sure the avatar directory of the current user is ready by;
             * checking if its readable ie. if it available,
             * if not create the folder
             */
            // user avatar path..
            $path = public_path().'/storage/admin/avatar/'.$lastId;

            try{
                //check if path exists
                if(!File::exists($path)) {
                    // path does not exist, create one..
                    File::makeDirectory($path, $mode = 0777, true, true);
                }
            } catch (Exception $exception ){
                $exception->getMessage();
            }
            if (empty($request->avatar)){
                // Leave empty...
                return Redirect()->back()->withErrors($validator)->withInput();
            }else {
                //Check for current avatar
                $avatar = $lastUser->avatar;
                $currentAvatar = $path . '/' . $avatar; // Current avatar
                // Update the user profile table and set the profile picture
                //If profile pix already exist
                if (File::exists($currentAvatar)) {
                    $photo = $request->avatar;
                    $fileNameExt = $photo->getClientOriginalName(); // get filename eg 123.jpg
                    $filename_new = $path . '/' . $fileNameExt;
                    Image::make($photo)->resize(250, 205)->save($filename_new);

                    User::where('id', '=', $lastUser->id)->update([
                        'avatar' => $fileNameExt,
                    ]);
                } else {
                    //else if doesnt exist, try inserting it into database..
                    $photo = $request->avatar;
                    $fileNameExt = $photo->getClientOriginalName();
                    $filename_new = $path . '/' . $fileNameExt;
                    Image::make($photo)->resize(250, 205)->save($filename_new);
                    User::where('id', '=', $lastUser->id)->update([
                        'avatar' => $fileNameExt,
                    ]);
                }
            }

            return redirect()->route('admin.list')->with("message", "Admin Updated");
        }
    }
    public function destroy($id) {
        $check = User::where('id', $id)->first();
        $deleted = $check->delete();
        if ($deleted) {
            return redirect()->route('admin.list')->with("message", "Post Deleted");
        } else {
            return redirect()->route('admin.list')->withErrors("Cannot delete post");
        }
    }
    public function profile(){
        return view('admin.admin.profile');
    }
    public function logout(Request $request)
    {
        $this->performLogout($request);
        return redirect()->route('admin.login');
    }
}
