import {setAuthorization} from "./general";

export function login(credentials) {
    return new Promise((res, rej) => {
        axios.post('/api/login', credentials)
            .then((response) => {
                setAuthorization(response.data.access_token);
                res(response.data);
            })
            .catch((error) => {
                //console.log(JSON.stringify(error.response.data.error));
                rej("Wrong Username or password");
            })
    })
}

export function register(credentials) {
    return new Promise((res, rej) => {
        axios.post('/api/register', credentials)
            .then((response) => {
                setAuthorization(response.data.access_token);
                res(response.data);
            })
            .catch((error) => {
                rej("Please check your form for error");
            })
    })
}

export function getLocalUser() {
    const userStr = localStorage.getItem("user");

    if (!userStr) {
        return null;
    }

    return JSON.parse(userStr);
}
