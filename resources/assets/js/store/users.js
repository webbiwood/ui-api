export default (api) => ({
    namespaced: true,
    state: {
        user: {},
        users: [],
    },
    actions: {
        getUser: ({commit, dispatch}, id) => {
            api.get('users', id)
                .then((response) => {
                    commit('setUser', response);
                })
                .catch((err) => console.log(err))
        },
        getUsers: ({commit, dispatch}, params) => {
            api.list('users', params)
                .then((response) => {
                    commit('setUsers', response);
                })
                .catch((err) => console.log(err));
        }
    },
    mutations: {
        setUser: (state, data) => state.user = data,
        setUsers: (state, data) => state.users = data
    },
    getters: {
        user: state => state.user,
        users: state => state.users,
    }
})
