<?php

namespace App\Http\Controllers;

use App\Channel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;

class ChannelController extends Controller
{
    public function index()
    {
        return Channel::all();
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        $channel = Channel::create($request->all());
        return $channel;
    }

    public function show($id)
    {
        return Channel::findOrFail($id);
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        $channel = Channel::findOrFail($id);
        $channel->update($request->all());

        return $channel;
    }

    public function destroy($id)
    {
        $channel = Channel::findOrFail($id);
        $channel->delete();
        return response(null, Response::HTTP_OK);
    }
}
