<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('user');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('password');
            $table->string('mobile', 20);
            $table->string('gender', 8);
            $table->enum('is_admin',[1,0])->default(0)->nullable();
            $table->enum('status',['active','inactive'])->nullable();
            $table->string('photoURL')->nullable();
            $table->string('lastLogin')->nullable();
            $table->string('recoverPass')->nullable();
            $table->string('backgroundURL')->nullable();
            $table->tinyInteger('canStream')->nullable();
            $table->tinyInteger('canUpload')->nullable();
            $table->string('channelName')->nullable();
            $table->tinyInteger('emailVerified')->nullable();
            $table->string('analyticsCode')->nullable();
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
