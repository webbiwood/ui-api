<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Video extends Model
{
    //use SoftDeletes;

    protected $fillable = [
        'title', 'clean_title', 'user_id', 'description', 'views_count', 'status','category_id','filename', 'duration', 'type',
        'videoDownloadedLink','order', 'rotation','zoom','youtubeId','videoLink', 'next_videos_id','isSuggested',
    ];

    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class,'user_id');
    }
}
