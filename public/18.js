webpackJsonp([18],{

/***/ 108:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(131)
}
var normalizeComponent = __webpack_require__(0)
/* script */
var __vue_script__ = __webpack_require__(133)
/* template */
var __vue_template__ = __webpack_require__(134)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-69da8bce"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/pages/Categories.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-69da8bce", Component.options)
  } else {
    hotAPI.reload("data-v-69da8bce", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 131:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(132);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(3)("7fd8afae", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-69da8bce\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./Categories.vue", function() {
     var newContent = require("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-69da8bce\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./Categories.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 132:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(2)(false);
// imports


// module
exports.push([module.i, "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n", ""]);

// exports


/***/ }),

/***/ 133:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
    name: "Category"
});

/***/ }),

/***/ 134:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _vm._m(0)
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "container-fluid" }, [
      _c("div", { staticClass: "video-block section-padding" }, [
        _c("div", { staticClass: "row" }, [
          _c("div", { staticClass: "col-md-12" }, [
            _c("div", { staticClass: "main-title" }, [
              _c("div", { staticClass: "btn-group float-right right-action" }, [
                _c(
                  "a",
                  {
                    staticClass: "right-action-link text-gray",
                    attrs: {
                      href: "#",
                      "data-toggle": "dropdown",
                      "aria-haspopup": "true",
                      "aria-expanded": "false"
                    }
                  },
                  [
                    _vm._v("\n                            Sort by "),
                    _c("i", {
                      staticClass: "fa fa-caret-down",
                      attrs: { "aria-hidden": "true" }
                    })
                  ]
                ),
                _vm._v(" "),
                _c(
                  "div",
                  { staticClass: "dropdown-menu dropdown-menu-right" },
                  [
                    _c(
                      "a",
                      { staticClass: "dropdown-item", attrs: { href: "#" } },
                      [
                        _c("i", { staticClass: "fas fa-fw fa-star" }),
                        _vm._v("   Top Rated")
                      ]
                    ),
                    _vm._v(" "),
                    _c(
                      "a",
                      { staticClass: "dropdown-item", attrs: { href: "#" } },
                      [
                        _c("i", { staticClass: "fas fa-fw fa-signal" }),
                        _vm._v("   Viewed")
                      ]
                    ),
                    _vm._v(" "),
                    _c(
                      "a",
                      { staticClass: "dropdown-item", attrs: { href: "#" } },
                      [
                        _c("i", { staticClass: "fas fa-fw fa-times-circle" }),
                        _vm._v("   Close")
                      ]
                    )
                  ]
                )
              ]),
              _vm._v(" "),
              _c("h6", [_vm._v("Categories")])
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "col-xl-3 col-sm-6 mb-3" }, [
            _c("div", { staticClass: "category-item mt-0 mb-0" }, [
              _c("a", { attrs: { href: "shop.html" } }, [
                _c("img", {
                  staticClass: "img-fluid",
                  attrs: { src: "img/s1.png", alt: "" }
                }),
                _vm._v(" "),
                _c("h6", [
                  _vm._v("Your Life "),
                  _c(
                    "span",
                    {
                      attrs: {
                        title: "",
                        "data-placement": "top",
                        "data-toggle": "tooltip",
                        "data-original-title": "Verified"
                      }
                    },
                    [
                      _c("i", {
                        staticClass: "fas fa-check-circle text-success"
                      })
                    ]
                  )
                ]),
                _vm._v(" "),
                _c("p", [_vm._v("74,853 views")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "col-xl-3 col-sm-6 mb-3" }, [
            _c("div", { staticClass: "category-item mt-0 mb-0" }, [
              _c("a", { attrs: { href: "shop.html" } }, [
                _c("img", {
                  staticClass: "img-fluid",
                  attrs: { src: "img/s2.png", alt: "" }
                }),
                _vm._v(" "),
                _c("h6", [
                  _vm._v("Unboxing Cool "),
                  _c(
                    "span",
                    {
                      attrs: {
                        title: "",
                        "data-placement": "top",
                        "data-toggle": "tooltip",
                        "data-original-title": "Verified"
                      }
                    },
                    [
                      _c("i", {
                        staticClass: "fas fa-check-circle text-success"
                      })
                    ]
                  )
                ]),
                _vm._v(" "),
                _c("p", [_vm._v("74,853 views")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "col-xl-3 col-sm-6 mb-3" }, [
            _c("div", { staticClass: "category-item mt-0 mb-0" }, [
              _c("a", { attrs: { href: "shop.html" } }, [
                _c("img", {
                  staticClass: "img-fluid",
                  attrs: { src: "img/s3.png", alt: "" }
                }),
                _vm._v(" "),
                _c("h6", [_vm._v("Service Reviewing")]),
                _vm._v(" "),
                _c("p", [_vm._v("74,853 views")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "col-xl-3 col-sm-6 mb-3" }, [
            _c("div", { staticClass: "category-item mt-0 mb-0" }, [
              _c("a", { attrs: { href: "shop.html" } }, [
                _c("img", {
                  staticClass: "img-fluid",
                  attrs: { src: "img/s4.png", alt: "" }
                }),
                _vm._v(" "),
                _c("h6", [
                  _vm._v("Gaming "),
                  _c(
                    "span",
                    {
                      attrs: {
                        title: "",
                        "data-placement": "top",
                        "data-toggle": "tooltip",
                        "data-original-title": "Verified"
                      }
                    },
                    [
                      _c("i", {
                        staticClass: "fas fa-check-circle text-success"
                      })
                    ]
                  )
                ]),
                _vm._v(" "),
                _c("p", [_vm._v("74,853 views")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "col-xl-3 col-sm-6 mb-3" }, [
            _c("div", { staticClass: "category-item mt-0 mb-0" }, [
              _c("a", { attrs: { href: "shop.html" } }, [
                _c("img", {
                  staticClass: "img-fluid",
                  attrs: { src: "img/s5.png", alt: "" }
                }),
                _vm._v(" "),
                _c("h6", [_vm._v("Technology Tutorials")]),
                _vm._v(" "),
                _c("p", [_vm._v("74,853 views")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "col-xl-3 col-sm-6 mb-3" }, [
            _c("div", { staticClass: "category-item mt-0 mb-0" }, [
              _c("a", { attrs: { href: "shop.html" } }, [
                _c("img", {
                  staticClass: "img-fluid",
                  attrs: { src: "img/s6.png", alt: "" }
                }),
                _vm._v(" "),
                _c("h6", [_vm._v("Singing")]),
                _vm._v(" "),
                _c("p", [_vm._v("74,853 views")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "col-xl-3 col-sm-6 mb-3" }, [
            _c("div", { staticClass: "category-item mt-0 mb-0" }, [
              _c("a", { attrs: { href: "shop.html" } }, [
                _c("img", {
                  staticClass: "img-fluid",
                  attrs: { src: "img/s7.png", alt: "" }
                }),
                _vm._v(" "),
                _c("h6", [_vm._v("Cooking")]),
                _vm._v(" "),
                _c("p", [_vm._v("74,853 views")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "col-xl-3 col-sm-6 mb-3" }, [
            _c("div", { staticClass: "category-item mt-0 mb-0" }, [
              _c("a", { attrs: { href: "shop.html" } }, [
                _c("img", {
                  staticClass: "img-fluid",
                  attrs: { src: "img/s8.png", alt: "" }
                }),
                _vm._v(" "),
                _c("h6", [_vm._v("Traveling")]),
                _vm._v(" "),
                _c("p", [_vm._v("74,853 views")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "col-xl-3 col-sm-6 mb-3" }, [
            _c("div", { staticClass: "category-item mt-0 mb-0" }, [
              _c("a", { attrs: { href: "shop.html" } }, [
                _c("img", {
                  staticClass: "img-fluid",
                  attrs: { src: "img/s1.png", alt: "" }
                }),
                _vm._v(" "),
                _c("h6", [_vm._v("Education")]),
                _vm._v(" "),
                _c("p", [_vm._v("74,853 views")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "col-xl-3 col-sm-6 mb-3" }, [
            _c("div", { staticClass: "category-item mt-0 mb-0" }, [
              _c("a", { attrs: { href: "shop.html" } }, [
                _c("img", {
                  staticClass: "img-fluid",
                  attrs: { src: "img/s2.png", alt: "" }
                }),
                _vm._v(" "),
                _c("h6", [_vm._v("Noodles, Sauces & Instant Food")]),
                _vm._v(" "),
                _c("p", [_vm._v("74,853 views")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "col-xl-3 col-sm-6 mb-3" }, [
            _c("div", { staticClass: "category-item mt-0 mb-0" }, [
              _c("a", { attrs: { href: "shop.html" } }, [
                _c("img", {
                  staticClass: "img-fluid",
                  attrs: { src: "img/s3.png", alt: "" }
                }),
                _vm._v(" "),
                _c("h6", [
                  _vm._v("Comedy "),
                  _c(
                    "span",
                    {
                      attrs: {
                        title: "",
                        "data-placement": "top",
                        "data-toggle": "tooltip",
                        "data-original-title": "Verified"
                      }
                    },
                    [
                      _c("i", {
                        staticClass: "fas fa-check-circle text-success"
                      })
                    ]
                  )
                ]),
                _vm._v(" "),
                _c("p", [_vm._v("74,853 views")])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "col-xl-3 col-sm-6 mb-3" }, [
            _c("div", { staticClass: "category-item mt-0 mb-0" }, [
              _c("a", { attrs: { href: "shop.html" } }, [
                _c("img", {
                  staticClass: "img-fluid",
                  attrs: { src: "img/s4.png", alt: "" }
                }),
                _vm._v(" "),
                _c("h6", [_vm._v("Lifestyle Advice")]),
                _vm._v(" "),
                _c("p", [_vm._v("74,853 views")])
              ])
            ])
          ])
        ]),
        _vm._v(" "),
        _c("nav", { attrs: { "aria-label": "Page navigation example" } }, [
          _c(
            "ul",
            {
              staticClass:
                "pagination justify-content-center pagination-sm mb-0"
            },
            [
              _c("li", { staticClass: "page-item disabled" }, [
                _c(
                  "a",
                  {
                    staticClass: "page-link",
                    attrs: { href: "#", tabindex: "-1" }
                  },
                  [_vm._v("Previous")]
                )
              ]),
              _vm._v(" "),
              _c("li", { staticClass: "page-item active" }, [
                _c("a", { staticClass: "page-link", attrs: { href: "#" } }, [
                  _vm._v("1")
                ])
              ]),
              _vm._v(" "),
              _c("li", { staticClass: "page-item" }, [
                _c("a", { staticClass: "page-link", attrs: { href: "#" } }, [
                  _vm._v("2")
                ])
              ]),
              _vm._v(" "),
              _c("li", { staticClass: "page-item" }, [
                _c("a", { staticClass: "page-link", attrs: { href: "#" } }, [
                  _vm._v("3")
                ])
              ]),
              _vm._v(" "),
              _c("li", { staticClass: "page-item" }, [
                _c("a", { staticClass: "page-link", attrs: { href: "#" } }, [
                  _vm._v("Next")
                ])
              ])
            ]
          )
        ])
      ])
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-69da8bce", module.exports)
  }
}

/***/ })

});