webpackJsonp([21],{

/***/ 103:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
var normalizeComponent = __webpack_require__(0)
/* script */
var __vue_script__ = __webpack_require__(113)
/* template */
var __vue_template__ = __webpack_require__(114)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/pages/Home.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-440dff1c", Component.options)
  } else {
    hotAPI.reload("data-v-440dff1c", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 113:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
    name: 'Home',
    data: function data() {
        return {
            videos: [],
            channels: [],
            categories: []
        };
    },
    beforeMount: function beforeMount() {
        var _this = this;

        axios.get("api/videos/").then(function (response) {
            return _this.videos = response.data;
        });
        //axios.get("api/videos/").then(response => this.videos = response.data)
    }
});

/***/ }),

/***/ 114:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "container-fluid pb-0" }, [
    _vm._m(0),
    _vm._v(" "),
    _c("hr"),
    _vm._v(" "),
    _c("div", { staticClass: "video-block section-padding" }, [
      _c("div", { staticClass: "row" }, [
        _vm._m(1),
        _vm._v(" "),
        _c("div", { staticClass: "col-xl-3 col-sm-6 mb-3" }, [
          _c("div", { staticClass: "video-card" }, [
            _c(
              "div",
              { staticClass: "video-card-image" },
              [
                _c(
                  "router-link",
                  {
                    staticClass: "play-icon",
                    attrs: { to: { name: "videos" } }
                  },
                  [_c("i", { staticClass: "fas fa-play-circle" })]
                ),
                _vm._v(" "),
                _vm._m(2),
                _vm._v(" "),
                _c("div", { staticClass: "time" }, [_vm._v("3:50")])
              ],
              1
            ),
            _vm._v(" "),
            _vm._m(3)
          ])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "col-xl-3 col-sm-6 mb-3" }, [
          _c("div", { staticClass: "video-card" }, [
            _c(
              "div",
              { staticClass: "video-card-image" },
              [
                _c(
                  "router-link",
                  {
                    staticClass: "play-icon",
                    attrs: { to: { name: "videos" } }
                  },
                  [_c("i", { staticClass: "fas fa-play-circle" })]
                ),
                _vm._v(" "),
                _vm._m(4),
                _vm._v(" "),
                _c("div", { staticClass: "time" }, [_vm._v("3:50")])
              ],
              1
            ),
            _vm._v(" "),
            _vm._m(5)
          ])
        ]),
        _vm._v(" "),
        _vm._m(6),
        _vm._v(" "),
        _vm._m(7),
        _vm._v(" "),
        _vm._m(8),
        _vm._v(" "),
        _vm._m(9),
        _vm._v(" "),
        _vm._m(10),
        _vm._v(" "),
        _vm._m(11)
      ])
    ]),
    _vm._v(" "),
    _c("hr", { staticClass: "mt-0" }),
    _vm._v(" "),
    _vm._m(12)
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "top-category section-padding mb-4" }, [
      _c("div", { staticClass: "row" }, [
        _c("div", { staticClass: "col-md-12" }, [
          _c("div", { staticClass: "main-title" }, [
            _c("div", { staticClass: "btn-group float-right right-action" }, [
              _c(
                "a",
                {
                  staticClass: "right-action-link text-gray",
                  attrs: {
                    href: "#",
                    "data-toggle": "dropdown",
                    "aria-haspopup": "true",
                    "aria-expanded": "false"
                  }
                },
                [
                  _c("i", {
                    staticClass: "fa fa-ellipsis-h",
                    attrs: { "aria-hidden": "true" }
                  })
                ]
              ),
              _vm._v(" "),
              _c("div", { staticClass: "dropdown-menu dropdown-menu-right" }, [
                _c(
                  "a",
                  { staticClass: "dropdown-item", attrs: { href: "#" } },
                  [
                    _c("i", { staticClass: "fas fa-fw fa-star" }),
                    _vm._v("   Top Rated")
                  ]
                ),
                _vm._v(" "),
                _c(
                  "a",
                  { staticClass: "dropdown-item", attrs: { href: "#" } },
                  [
                    _c("i", { staticClass: "fas fa-fw fa-signal" }),
                    _vm._v("   Viewed")
                  ]
                ),
                _vm._v(" "),
                _c(
                  "a",
                  { staticClass: "dropdown-item", attrs: { href: "#" } },
                  [
                    _c("i", { staticClass: "fas fa-fw fa-times-circle" }),
                    _vm._v("  \n\t\t\t\t\t\t\t\tClose")
                  ]
                )
              ])
            ]),
            _vm._v(" "),
            _c("h6", [_vm._v("Channels Categories")])
          ])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "col-md-12" }, [
          _c("div", { staticClass: "owl-carousel owl-carousel-category" }, [
            _c("div", { staticClass: "item" }, [
              _c("div", { staticClass: "category-item" }, [
                _c("a", { attrs: { href: "#" } }, [
                  _c("img", {
                    staticClass: "img-fluid",
                    attrs: { src: "img/s1.png", alt: "" }
                  }),
                  _vm._v(" "),
                  _c("h6", [_vm._v("Your Life")]),
                  _vm._v(" "),
                  _c("p", [_vm._v("74,853 views")])
                ])
              ])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "item" }, [
              _c("div", { staticClass: "category-item" }, [
                _c("a", { attrs: { href: "#" } }, [
                  _c("img", {
                    staticClass: "img-fluid",
                    attrs: { src: "img/s2.png", alt: "" }
                  }),
                  _vm._v(" "),
                  _c("h6", [_vm._v("Unboxing Cool")]),
                  _vm._v(" "),
                  _c("p", [_vm._v("74,853 views")])
                ])
              ])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "item" }, [
              _c("div", { staticClass: "category-item" }, [
                _c("a", { attrs: { href: "#" } }, [
                  _c("img", {
                    staticClass: "img-fluid",
                    attrs: { src: "img/s3.png", alt: "" }
                  }),
                  _vm._v(" "),
                  _c("h6", [_vm._v("Service Reviewing")]),
                  _vm._v(" "),
                  _c("p", [_vm._v("74,853 views")])
                ])
              ])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "item" }, [
              _c("div", { staticClass: "category-item" }, [
                _c("a", { attrs: { href: "#" } }, [
                  _c("img", {
                    staticClass: "img-fluid",
                    attrs: { src: "img/s4.png", alt: "" }
                  }),
                  _vm._v(" "),
                  _c("h6", [
                    _vm._v("Gaming "),
                    _c(
                      "span",
                      {
                        attrs: {
                          title: "",
                          "data-placement": "top",
                          "data-toggle": "tooltip",
                          "data-original-title": "Verified"
                        }
                      },
                      [
                        _c("i", {
                          staticClass: "fas fa-check-circle text-success"
                        })
                      ]
                    )
                  ]),
                  _vm._v(" "),
                  _c("p", [_vm._v("74,853 views")])
                ])
              ])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "item" }, [
              _c("div", { staticClass: "category-item" }, [
                _c("a", { attrs: { href: "#" } }, [
                  _c("img", {
                    staticClass: "img-fluid",
                    attrs: { src: "img/s5.png", alt: "" }
                  }),
                  _vm._v(" "),
                  _c("h6", [_vm._v("Technology Tutorials")]),
                  _vm._v(" "),
                  _c("p", [_vm._v("74,853 views")])
                ])
              ])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "item" }, [
              _c("div", { staticClass: "category-item" }, [
                _c("a", { attrs: { href: "#" } }, [
                  _c("img", {
                    staticClass: "img-fluid",
                    attrs: { src: "img/s6.png", alt: "" }
                  }),
                  _vm._v(" "),
                  _c("h6", [_vm._v("Singing")]),
                  _vm._v(" "),
                  _c("p", [_vm._v("74,853 views")])
                ])
              ])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "item" }, [
              _c("div", { staticClass: "category-item" }, [
                _c("a", { attrs: { href: "#" } }, [
                  _c("img", {
                    staticClass: "img-fluid",
                    attrs: { src: "img/s7.png", alt: "" }
                  }),
                  _vm._v(" "),
                  _c("h6", [_vm._v("Cooking")]),
                  _vm._v(" "),
                  _c("p", [_vm._v("74,853 views")])
                ])
              ])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "item" }, [
              _c("div", { staticClass: "category-item" }, [
                _c("a", { attrs: { href: "#" } }, [
                  _c("img", {
                    staticClass: "img-fluid",
                    attrs: { src: "img/s8.png", alt: "" }
                  }),
                  _vm._v(" "),
                  _c("h6", [_vm._v("Traveling")]),
                  _vm._v(" "),
                  _c("p", [_vm._v("74,853 views")])
                ])
              ])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "item" }, [
              _c("div", { staticClass: "category-item" }, [
                _c("a", { attrs: { href: "#" } }, [
                  _c("img", {
                    staticClass: "img-fluid",
                    attrs: { src: "img/s1.png", alt: "" }
                  }),
                  _vm._v(" "),
                  _c("h6", [_vm._v("Education")]),
                  _vm._v(" "),
                  _c("p", [_vm._v("74,853 views")])
                ])
              ])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "item" }, [
              _c("div", { staticClass: "category-item" }, [
                _c("a", { attrs: { href: "#" } }, [
                  _c("img", {
                    staticClass: "img-fluid",
                    attrs: { src: "img/s2.png", alt: "" }
                  }),
                  _vm._v(" "),
                  _c("h6", [_vm._v("Noodles, Sauces & Instant Food")]),
                  _vm._v(" "),
                  _c("p", [_vm._v("74,853 views")])
                ])
              ])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "item" }, [
              _c("div", { staticClass: "category-item" }, [
                _c("a", { attrs: { href: "#" } }, [
                  _c("img", {
                    staticClass: "img-fluid",
                    attrs: { src: "img/s3.png", alt: "" }
                  }),
                  _vm._v(" "),
                  _c("h6", [
                    _vm._v("Comedy "),
                    _c(
                      "span",
                      {
                        attrs: {
                          title: "",
                          "data-placement": "top",
                          "data-toggle": "tooltip",
                          "data-original-title": "Verified"
                        }
                      },
                      [
                        _c("i", {
                          staticClass: "fas fa-check-circle text-success"
                        })
                      ]
                    )
                  ]),
                  _vm._v(" "),
                  _c("p", [_vm._v("74,853 views")])
                ])
              ])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "item" }, [
              _c("div", { staticClass: "category-item" }, [
                _c("a", { attrs: { href: "#" } }, [
                  _c("img", {
                    staticClass: "img-fluid",
                    attrs: { src: "img/s4.png", alt: "" }
                  }),
                  _vm._v(" "),
                  _c("h6", [_vm._v("Lifestyle Advice")]),
                  _vm._v(" "),
                  _c("p", [_vm._v("74,853 views")])
                ])
              ])
            ])
          ])
        ])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "col-md-12" }, [
      _c("div", { staticClass: "main-title" }, [
        _c("div", { staticClass: "btn-group float-right right-action" }, [
          _c(
            "a",
            {
              staticClass: "right-action-link text-gray",
              attrs: {
                href: "#",
                "data-toggle": "dropdown",
                "aria-haspopup": "true",
                "aria-expanded": "false"
              }
            },
            [
              _vm._v("\n\t\t\t\t\t\t\tSort by "),
              _c("i", {
                staticClass: "fa fa-caret-down",
                attrs: { "aria-hidden": "true" }
              })
            ]
          ),
          _vm._v(" "),
          _c("div", { staticClass: "dropdown-menu dropdown-menu-right" }, [
            _c("a", { staticClass: "dropdown-item", attrs: { href: "#" } }, [
              _c("i", { staticClass: "fas fa-fw fa-star" }),
              _vm._v("   Top Rated")
            ]),
            _vm._v(" "),
            _c("a", { staticClass: "dropdown-item", attrs: { href: "#" } }, [
              _c("i", { staticClass: "fas fa-fw fa-signal" }),
              _vm._v("   Viewed")
            ]),
            _vm._v(" "),
            _c("a", { staticClass: "dropdown-item", attrs: { href: "#" } }, [
              _c("i", { staticClass: "fas fa-fw fa-times-circle" }),
              _vm._v("  \n\t\t\t\t\t\t\t\tClose")
            ])
          ])
        ]),
        _vm._v(" "),
        _c("h6", [_vm._v("Featured Videos")])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("a", { attrs: { href: "#" } }, [
      _c("img", {
        staticClass: "img-fluid",
        attrs: { src: "img/v1.png", alt: "" }
      })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "video-card-body" }, [
      _c("div", { staticClass: "video-title" }, [
        _c("a", { attrs: { href: "#" } }, [
          _vm._v("There are many variations of passages of Lorem")
        ])
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "video-page text-success" }, [
        _vm._v("\n\t\t\t\t\t\t\tEducation "),
        _c(
          "a",
          {
            attrs: {
              title: "",
              "data-placement": "top",
              "data-toggle": "tooltip",
              href: "#",
              "data-original-title": "Verified"
            }
          },
          [_c("i", { staticClass: "fas fa-check-circle text-success" })]
        )
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "video-view" }, [
        _vm._v("\n\t\t\t\t\t\t\t1.8M views  "),
        _c("i", { staticClass: "fas fa-calendar-alt" }),
        _vm._v(" 11 Months ago\n\t\t\t\t\t\t")
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("a", { attrs: { href: "#" } }, [
      _c("img", {
        staticClass: "img-fluid",
        attrs: { src: "img/v2.png", alt: "" }
      })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "video-card-body" }, [
      _c("div", { staticClass: "video-title" }, [
        _c("a", { attrs: { href: "#" } }, [
          _vm._v("There are many variations of passages of Lorem")
        ])
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "video-page text-success" }, [
        _vm._v("\n\t\t\t\t\t\t\tEducation "),
        _c(
          "a",
          {
            attrs: {
              title: "",
              "data-placement": "top",
              "data-toggle": "tooltip",
              href: "#",
              "data-original-title": "Verified"
            }
          },
          [_c("i", { staticClass: "fas fa-check-circle text-success" })]
        )
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "video-view" }, [
        _vm._v("\n\t\t\t\t\t\t\t1.8M views  "),
        _c("i", { staticClass: "fas fa-calendar-alt" }),
        _vm._v(" 11 Months ago\n\t\t\t\t\t\t")
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "col-xl-3 col-sm-6 mb-3" }, [
      _c("div", { staticClass: "video-card" }, [
        _c("div", { staticClass: "video-card-image" }, [
          _c("a", { staticClass: "play-icon", attrs: { href: "#" } }, [
            _c("i", { staticClass: "fas fa-play-circle" })
          ]),
          _vm._v(" "),
          _c("a", { attrs: { href: "#" } }, [
            _c("img", {
              staticClass: "img-fluid",
              attrs: { src: "img/v3.png", alt: "" }
            })
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "time" }, [_vm._v("3:50")])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "video-card-body" }, [
          _c("div", { staticClass: "video-title" }, [
            _c("a", { attrs: { href: "#" } }, [
              _vm._v("There are many variations of passages of Lorem")
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "video-page text-danger" }, [
            _vm._v("\n\t\t\t\t\t\t\tEducation "),
            _c(
              "a",
              {
                attrs: {
                  title: "",
                  "data-placement": "top",
                  "data-toggle": "tooltip",
                  href: "#",
                  "data-original-title": "Unverified"
                }
              },
              [_c("i", { staticClass: "fas fa-frown text-danger" })]
            )
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "video-view" }, [
            _vm._v("\n\t\t\t\t\t\t\t1.8M views  "),
            _c("i", { staticClass: "fas fa-calendar-alt" }),
            _vm._v(" 11 Months ago\n\t\t\t\t\t\t")
          ])
        ])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "col-xl-3 col-sm-6 mb-3" }, [
      _c("div", { staticClass: "video-card" }, [
        _c("div", { staticClass: "video-card-image" }, [
          _c("a", { staticClass: "play-icon", attrs: { href: "#" } }, [
            _c("i", { staticClass: "fas fa-play-circle" })
          ]),
          _vm._v(" "),
          _c("a", { attrs: { href: "#" } }, [
            _c("img", {
              staticClass: "img-fluid",
              attrs: { src: "img/v4.png", alt: "" }
            })
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "time" }, [_vm._v("3:50")])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "video-card-body" }, [
          _c("div", { staticClass: "video-title" }, [
            _c("a", { attrs: { href: "#" } }, [
              _vm._v("There are many variations of passages of Lorem")
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "video-page text-success" }, [
            _vm._v("\n\t\t\t\t\t\t\tEducation "),
            _c(
              "a",
              {
                attrs: {
                  title: "",
                  "data-placement": "top",
                  "data-toggle": "tooltip",
                  href: "#",
                  "data-original-title": "Verified"
                }
              },
              [_c("i", { staticClass: "fas fa-check-circle text-success" })]
            )
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "video-view" }, [
            _vm._v("\n\t\t\t\t\t\t\t1.8M views  "),
            _c("i", { staticClass: "fas fa-calendar-alt" }),
            _vm._v(" 11 Months ago\n\t\t\t\t\t\t")
          ])
        ])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "col-xl-3 col-sm-6 mb-3" }, [
      _c("div", { staticClass: "video-card" }, [
        _c("div", { staticClass: "video-card-image" }, [
          _c("a", { staticClass: "play-icon", attrs: { href: "#" } }, [
            _c("i", { staticClass: "fas fa-play-circle" })
          ]),
          _vm._v(" "),
          _c("a", { attrs: { href: "#" } }, [
            _c("img", {
              staticClass: "img-fluid",
              attrs: { src: "img/v5.png", alt: "" }
            })
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "time" }, [_vm._v("3:50")])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "video-card-body" }, [
          _c("div", { staticClass: "video-title" }, [
            _c("a", { attrs: { href: "#" } }, [
              _vm._v("There are many variations of passages of Lorem")
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "video-page text-success" }, [
            _vm._v("\n\t\t\t\t\t\t\tEducation "),
            _c(
              "a",
              {
                attrs: {
                  title: "",
                  "data-placement": "top",
                  "data-toggle": "tooltip",
                  href: "#",
                  "data-original-title": "Verified"
                }
              },
              [_c("i", { staticClass: "fas fa-check-circle text-success" })]
            )
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "video-view" }, [
            _vm._v("\n\t\t\t\t\t\t\t1.8M views  "),
            _c("i", { staticClass: "fas fa-calendar-alt" }),
            _vm._v(" 11 Months ago\n\t\t\t\t\t\t")
          ])
        ])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "col-xl-3 col-sm-6 mb-3" }, [
      _c("div", { staticClass: "video-card" }, [
        _c("div", { staticClass: "video-card-image" }, [
          _c("a", { staticClass: "play-icon", attrs: { href: "#" } }, [
            _c("i", { staticClass: "fas fa-play-circle" })
          ]),
          _vm._v(" "),
          _c("a", { attrs: { href: "#" } }, [
            _c("img", {
              staticClass: "img-fluid",
              attrs: { src: "img/v6.png", alt: "" }
            })
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "time" }, [_vm._v("3:50")])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "video-card-body" }, [
          _c("div", { staticClass: "video-title" }, [
            _c("a", { attrs: { href: "#" } }, [
              _vm._v("There are many variations of passages of Lorem")
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "video-page text-danger" }, [
            _vm._v("\n\t\t\t\t\t\t\tEducation "),
            _c(
              "a",
              {
                attrs: {
                  title: "",
                  "data-placement": "top",
                  "data-toggle": "tooltip",
                  href: "#",
                  "data-original-title": "Unverified"
                }
              },
              [_c("i", { staticClass: "fas fa-frown text-danger" })]
            )
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "video-view" }, [
            _vm._v("\n\t\t\t\t\t\t\t1.8M views  "),
            _c("i", { staticClass: "fas fa-calendar-alt" }),
            _vm._v(" 11 Months ago\n\t\t\t\t\t\t")
          ])
        ])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "col-xl-3 col-sm-6 mb-3" }, [
      _c("div", { staticClass: "video-card" }, [
        _c("div", { staticClass: "video-card-image" }, [
          _c("a", { staticClass: "play-icon", attrs: { href: "#" } }, [
            _c("i", { staticClass: "fas fa-play-circle" })
          ]),
          _vm._v(" "),
          _c("a", { attrs: { href: "#" } }, [
            _c("img", {
              staticClass: "img-fluid",
              attrs: { src: "img/v7.png", alt: "" }
            })
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "time" }, [_vm._v("3:50")])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "video-card-body" }, [
          _c("div", { staticClass: "video-title" }, [
            _c("a", { attrs: { href: "#" } }, [
              _vm._v("There are many variations of passages of Lorem")
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "video-page text-success" }, [
            _vm._v("\n\t\t\t\t\t\t\tEducation "),
            _c(
              "a",
              {
                attrs: {
                  title: "",
                  "data-placement": "top",
                  "data-toggle": "tooltip",
                  href: "#",
                  "data-original-title": "Verified"
                }
              },
              [_c("i", { staticClass: "fas fa-check-circle text-success" })]
            )
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "video-view" }, [
            _vm._v("\n\t\t\t\t\t\t\t1.8M views  "),
            _c("i", { staticClass: "fas fa-calendar-alt" }),
            _vm._v(" 11 Months ago\n\t\t\t\t\t\t")
          ])
        ])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "col-xl-3 col-sm-6 mb-3" }, [
      _c("div", { staticClass: "video-card" }, [
        _c("div", { staticClass: "video-card-image" }, [
          _c("a", { staticClass: "play-icon", attrs: { href: "#" } }, [
            _c("i", { staticClass: "fas fa-play-circle" })
          ]),
          _vm._v(" "),
          _c("a", { attrs: { href: "#" } }, [
            _c("img", {
              staticClass: "img-fluid",
              attrs: { src: "img/v8.png", alt: "" }
            })
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "time" }, [_vm._v("3:50")])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "video-card-body" }, [
          _c("div", { staticClass: "video-title" }, [
            _c("a", { attrs: { href: "#" } }, [
              _vm._v("There are many variations of passages of Lorem")
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "video-page text-success" }, [
            _vm._v("\n\t\t\t\t\t\t\tEducation "),
            _c(
              "a",
              {
                attrs: {
                  title: "",
                  "data-placement": "top",
                  "data-toggle": "tooltip",
                  href: "#",
                  "data-original-title": "Verified"
                }
              },
              [_c("i", { staticClass: "fas fa-check-circle text-success" })]
            )
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "video-view" }, [
            _vm._v("\n\t\t\t\t\t\t\t1.8M views  "),
            _c("i", { staticClass: "fas fa-calendar-alt" }),
            _vm._v(" 11 Months ago\n\t\t\t\t\t\t")
          ])
        ])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "video-block section-padding" }, [
      _c("div", { staticClass: "row" }, [
        _c("div", { staticClass: "col-md-12" }, [
          _c("div", { staticClass: "main-title" }, [
            _c("div", { staticClass: "btn-group float-right right-action" }, [
              _c(
                "a",
                {
                  staticClass: "right-action-link text-gray",
                  attrs: {
                    href: "#",
                    "data-toggle": "dropdown",
                    "aria-haspopup": "true",
                    "aria-expanded": "false"
                  }
                },
                [
                  _vm._v("\n\t\t\t\t\t\t\tSort by "),
                  _c("i", {
                    staticClass: "fa fa-caret-down",
                    attrs: { "aria-hidden": "true" }
                  })
                ]
              ),
              _vm._v(" "),
              _c("div", { staticClass: "dropdown-menu dropdown-menu-right" }, [
                _c(
                  "a",
                  { staticClass: "dropdown-item", attrs: { href: "#" } },
                  [
                    _c("i", { staticClass: "fas fa-fw fa-star" }),
                    _vm._v("   Top Rated")
                  ]
                ),
                _vm._v(" "),
                _c(
                  "a",
                  { staticClass: "dropdown-item", attrs: { href: "#" } },
                  [
                    _c("i", { staticClass: "fas fa-fw fa-signal" }),
                    _vm._v("   Viewed")
                  ]
                ),
                _vm._v(" "),
                _c(
                  "a",
                  { staticClass: "dropdown-item", attrs: { href: "#" } },
                  [
                    _c("i", { staticClass: "fas fa-fw fa-times-circle" }),
                    _vm._v("  \n\t\t\t\t\t\t\t\tClose")
                  ]
                )
              ])
            ]),
            _vm._v(" "),
            _c("h6", [_vm._v("Popular Channels")])
          ])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "col-xl-3 col-sm-6 mb-3" }, [
          _c("div", { staticClass: "channels-card" }, [
            _c("div", { staticClass: "channels-card-image" }, [
              _c("a", { attrs: { href: "#" } }, [
                _c("img", {
                  staticClass: "img-fluid",
                  attrs: { src: "img/s1.png", alt: "" }
                })
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "channels-card-image-btn" }, [
                _c(
                  "button",
                  {
                    staticClass: "btn btn-outline-danger btn-sm",
                    attrs: { type: "button" }
                  },
                  [
                    _vm._v("Subscribe\n\t\t\t\t\t\t\t\t"),
                    _c("strong", [_vm._v("1.4M")])
                  ]
                )
              ])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "channels-card-body" }, [
              _c("div", { staticClass: "channels-title" }, [
                _c("a", { attrs: { href: "#" } }, [_vm._v("Channels Name")])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "channels-view" }, [
                _vm._v("\n\t\t\t\t\t\t\t382,323 subscribers\n\t\t\t\t\t\t")
              ])
            ])
          ])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "col-xl-3 col-sm-6 mb-3" }, [
          _c("div", { staticClass: "channels-card" }, [
            _c("div", { staticClass: "channels-card-image" }, [
              _c("a", { attrs: { href: "#" } }, [
                _c("img", {
                  staticClass: "img-fluid",
                  attrs: { src: "img/s2.png", alt: "" }
                })
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "channels-card-image-btn" }, [
                _c(
                  "button",
                  {
                    staticClass: "btn btn-outline-danger btn-sm",
                    attrs: { type: "button" }
                  },
                  [
                    _vm._v("Subscribe\n\t\t\t\t\t\t\t\t"),
                    _c("strong", [_vm._v("1.4M")])
                  ]
                )
              ])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "channels-card-body" }, [
              _c("div", { staticClass: "channels-title" }, [
                _c("a", { attrs: { href: "#" } }, [_vm._v("Channels Name")])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "channels-view" }, [
                _vm._v("\n\t\t\t\t\t\t\t382,323 subscribers\n\t\t\t\t\t\t")
              ])
            ])
          ])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "col-xl-3 col-sm-6 mb-3" }, [
          _c("div", { staticClass: "channels-card" }, [
            _c("div", { staticClass: "channels-card-image" }, [
              _c("a", { attrs: { href: "#" } }, [
                _c("img", {
                  staticClass: "img-fluid",
                  attrs: { src: "img/s3.png", alt: "" }
                })
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "channels-card-image-btn" }, [
                _c(
                  "button",
                  {
                    staticClass: "btn btn-outline-secondary btn-sm",
                    attrs: { type: "button" }
                  },
                  [_vm._v("Subscribed "), _c("strong", [_vm._v("1.4M")])]
                )
              ])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "channels-card-body" }, [
              _c("div", { staticClass: "channels-title" }, [
                _c("a", { attrs: { href: "#" } }, [
                  _vm._v("Channels Name "),
                  _c(
                    "span",
                    {
                      attrs: {
                        title: "",
                        "data-placement": "top",
                        "data-toggle": "tooltip",
                        "data-original-title": "Verified"
                      }
                    },
                    [_c("i", { staticClass: "fas fa-check-circle" })]
                  )
                ])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "channels-view" }, [
                _vm._v("\n\t\t\t\t\t\t\t382,323 subscribers\n\t\t\t\t\t\t")
              ])
            ])
          ])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "col-xl-3 col-sm-6 mb-3" }, [
          _c("div", { staticClass: "channels-card" }, [
            _c("div", { staticClass: "channels-card-image" }, [
              _c("a", { attrs: { href: "#" } }, [
                _c("img", {
                  staticClass: "img-fluid",
                  attrs: { src: "img/s4.png", alt: "" }
                })
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "channels-card-image-btn" }, [
                _c(
                  "button",
                  {
                    staticClass: "btn btn-outline-danger btn-sm",
                    attrs: { type: "button" }
                  },
                  [
                    _vm._v("Subscribe\n\t\t\t\t\t\t\t\t"),
                    _c("strong", [_vm._v("1.4M")])
                  ]
                )
              ])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "channels-card-body" }, [
              _c("div", { staticClass: "channels-title" }, [
                _c("a", { attrs: { href: "#" } }, [_vm._v("Channels Name")])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "channels-view" }, [
                _vm._v("\n\t\t\t\t\t\t\t382,323 subscribers\n\t\t\t\t\t\t")
              ])
            ])
          ])
        ])
      ])
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-440dff1c", module.exports)
  }
}

/***/ })

});