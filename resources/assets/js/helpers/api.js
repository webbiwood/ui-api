import axios from 'axios';
import Form from 'form-data';

const API_INSTANCE = axios.create({
    baseURL: '/api',

});


const composeFormData = (data) => {
    let form = new Form();

    for (let i in data) {
        form.append(i, data[i]);
    }
    return form;
};

//TODO: Change the endpoints for each endpoint.

export default {
    list: async (endpoint, params) =>
        await API_INSTANCE.get(`/${endpoint}/show/`, {
            headers: {}
        }),
    get: async (endpoint, id) =>
        await API_INSTANCE.get(`/${endpoint}/show/${id}`, {
            headers: {}
        }),
    delete: async (endpoint, id) =>
        await API_INSTANCE.delete(`/${endpoint}/show/${id}`, {
            headers: {}
        }),
    create: async (endpoint, data) =>
        await API_INSTANCE.post(`/${endpoint}/show/${id}`, composeFormData(data), {
            headers: {}
        }),
    edit: async (endpoint, data) =>
        await API_INSTANCE.post(`/${endpoint}/edit/${data.id}`, composeFormData(data), {
            headers: {}
        }),
}
