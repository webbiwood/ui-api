<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Webbiwood - ...from obscurity to limelight</title>
    <!-- Favicon Icon -->
    <link rel="icon" type="image/png" href="img/favicon.png">
    <!-- Bootstrap core CSS-->
    <link href="{{ asset('vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
    <!-- Custom fonts for this template-->
    <link href="{{ asset('vendor/fontawesome-free/css/all.min.css') }}" rel="stylesheet" type="text/css">
    <!-- Custom styles for this template-->
    <link href="{{ asset('css/webbiwood.css') }}" rel="stylesheet">
    <!-- Owl Carousel -->
    <link rel="stylesheet" href="{{ asset('/vendor/owl-carousel/owl.carousel.css') }}">
    <link rel="stylesheet" href="{{ asset('/vendor/owl-carousel/owl.theme.css') }}">

</head>
<body>
<div id="page-top">
    <app></app>
</div>
<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
</a>
<logout></logout>
<script src="{{ mix('js/app.js') }}"></script>
<!-- ================== BEGIN BASE JS ================== -->
<!-- Bootstrap core JavaScript-->
<!--<script src="{{-- asset('vendor/jquery/jquery.min.js') --}}"></script>-->
<!-- <script src="{{-- asset('vendor/bootstrap/js/bootstrap.bundle.min.js') --}}"></script>-->
<!-- Core plugin JavaScript-->
<script src="{{ asset('vendor/jquery-easing/jquery.easing.min.js') }}"></script>
<!-- Owl Carousel -->
<script src="{{ asset('vendor/owl-carousel/owl.carousel.js') }}"></script>
<!-- Custom scripts for all pages-->
<script src="{{ asset('js/custom.js') }}"></script>
{{-- END BASE JS--}}
</body>

</html>
