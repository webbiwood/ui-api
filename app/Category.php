<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
//use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    //use SoftDeletes;
    public $timestamps = false;

    protected $fillable = [
        'name','clean_name','description','iconClass','nextVideoOrder','parentId'
    ];

    public function videos()
    {
        return $this->hasMany(Video::class);
    }
}
