webpackJsonp([11],{

/***/ 110:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
var normalizeComponent = __webpack_require__(0)
/* script */
var __vue_script__ = __webpack_require__(122)
/* template */
var __vue_template__ = __webpack_require__(123)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/pages/Home.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-440dff1c", Component.options)
  } else {
    hotAPI.reload("data-v-440dff1c", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 122:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__components_VideoPlayer__ = __webpack_require__(190);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__components_VideoPlayer___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__components_VideoPlayer__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//



/* harmony default export */ __webpack_exports__["default"] = ({
    name: 'Home',
    components: { BigVideoPlayer: __WEBPACK_IMPORTED_MODULE_0__components_VideoPlayer___default.a },
    data: function data() {
        return {
            videos: [],
            channels: [],
            categories: []
        };
    },
    beforeMount: function beforeMount() {
        var _this = this;

        axios.get("api/videos/").then(function (response) {
            return _this.videos = response.data;
        });
        //axios.get("api/videos/").then(response => this.videos = response.data)
    }
});

/***/ }),

/***/ 123:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "container-fluid pb-0" },
    [
      _c("BigVideoPlayer"),
      _vm._v(" "),
      _vm._m(0),
      _vm._v(" "),
      _c("hr"),
      _vm._v(" "),
      _c("div", { staticClass: "video-block section-padding" }, [
        _c("div", { staticClass: "row" }, [
          _vm._m(1),
          _vm._v(" "),
          _c("div", { staticClass: "col-xl-3 col-sm-6 mb-3" }, [
            _c("div", { staticClass: "video-card" }, [
              _c(
                "div",
                { staticClass: "video-card-image" },
                [
                  _c(
                    "router-link",
                    {
                      staticClass: "play-icon",
                      attrs: { to: { name: "videos" } }
                    },
                    [_c("i", { staticClass: "fas fa-play-circle" })]
                  ),
                  _vm._v(" "),
                  _vm._m(2),
                  _vm._v(" "),
                  _c("div", { staticClass: "time" }, [_vm._v("3:50")])
                ],
                1
              ),
              _vm._v(" "),
              _vm._m(3)
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "col-xl-3 col-sm-6 mb-3" }, [
            _c("div", { staticClass: "video-card" }, [
              _c(
                "div",
                { staticClass: "video-card-image" },
                [
                  _c(
                    "router-link",
                    {
                      staticClass: "play-icon",
                      attrs: { to: { name: "videos" } }
                    },
                    [_c("i", { staticClass: "fas fa-play-circle" })]
                  ),
                  _vm._v(" "),
                  _vm._m(4),
                  _vm._v(" "),
                  _c("div", { staticClass: "time" }, [_vm._v("3:50")])
                ],
                1
              ),
              _vm._v(" "),
              _vm._m(5)
            ])
          ]),
          _vm._v(" "),
          _vm._m(6),
          _vm._v(" "),
          _vm._m(7),
          _vm._v(" "),
          _vm._m(8),
          _vm._v(" "),
          _vm._m(9),
          _vm._v(" "),
          _vm._m(10),
          _vm._v(" "),
          _vm._m(11)
        ])
      ]),
      _vm._v(" "),
      _c("hr", { staticClass: "mt-0" }),
      _vm._v(" "),
      _vm._m(12)
    ],
    1
  )
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "top-category section-padding mb-4" }, [
      _c("div", { staticClass: "row" }, [
        _c("div", { staticClass: "col-md-12" }, [
          _c("div", { staticClass: "main-title" }, [
            _c("div", { staticClass: "btn-group float-right right-action" }, [
              _c(
                "a",
                {
                  staticClass: "right-action-link text-gray",
                  attrs: {
                    href: "#",
                    "data-toggle": "dropdown",
                    "aria-haspopup": "true",
                    "aria-expanded": "false"
                  }
                },
                [
                  _c("i", {
                    staticClass: "fa fa-ellipsis-h",
                    attrs: { "aria-hidden": "true" }
                  })
                ]
              ),
              _vm._v(" "),
              _c("div", { staticClass: "dropdown-menu dropdown-menu-right" }, [
                _c(
                  "a",
                  { staticClass: "dropdown-item", attrs: { href: "#" } },
                  [
                    _c("i", { staticClass: "fas fa-fw fa-star" }),
                    _vm._v("   Top Rated")
                  ]
                ),
                _vm._v(" "),
                _c(
                  "a",
                  { staticClass: "dropdown-item", attrs: { href: "#" } },
                  [
                    _c("i", { staticClass: "fas fa-fw fa-signal" }),
                    _vm._v("   Viewed")
                  ]
                ),
                _vm._v(" "),
                _c(
                  "a",
                  { staticClass: "dropdown-item", attrs: { href: "#" } },
                  [
                    _c("i", { staticClass: "fas fa-fw fa-times-circle" }),
                    _vm._v("  \n                                Close")
                  ]
                )
              ])
            ]),
            _vm._v(" "),
            _c("h6", [_vm._v("Channels Categories")])
          ])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "col-md-12" }, [
          _c("div", { staticClass: "owl-carousel owl-carousel-category" }, [
            _c("div", { staticClass: "item" }, [
              _c("div", { staticClass: "category-item" }, [
                _c("a", { attrs: { href: "#" } }, [
                  _c("img", {
                    staticClass: "img-fluid",
                    attrs: { src: "img/s1.png", alt: "" }
                  }),
                  _vm._v(" "),
                  _c("h6", [_vm._v("Your Life")]),
                  _vm._v(" "),
                  _c("p", [_vm._v("74,853 views")])
                ])
              ])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "item" }, [
              _c("div", { staticClass: "category-item" }, [
                _c("a", { attrs: { href: "#" } }, [
                  _c("img", {
                    staticClass: "img-fluid",
                    attrs: { src: "img/s2.png", alt: "" }
                  }),
                  _vm._v(" "),
                  _c("h6", [_vm._v("Unboxing Cool")]),
                  _vm._v(" "),
                  _c("p", [_vm._v("74,853 views")])
                ])
              ])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "item" }, [
              _c("div", { staticClass: "category-item" }, [
                _c("a", { attrs: { href: "#" } }, [
                  _c("img", {
                    staticClass: "img-fluid",
                    attrs: { src: "img/s3.png", alt: "" }
                  }),
                  _vm._v(" "),
                  _c("h6", [_vm._v("Service Reviewing")]),
                  _vm._v(" "),
                  _c("p", [_vm._v("74,853 views")])
                ])
              ])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "item" }, [
              _c("div", { staticClass: "category-item" }, [
                _c("a", { attrs: { href: "#" } }, [
                  _c("img", {
                    staticClass: "img-fluid",
                    attrs: { src: "img/s4.png", alt: "" }
                  }),
                  _vm._v(" "),
                  _c("h6", [
                    _vm._v("Gaming "),
                    _c(
                      "span",
                      {
                        attrs: {
                          title: "",
                          "data-placement": "top",
                          "data-toggle": "tooltip",
                          "data-original-title": "Verified"
                        }
                      },
                      [
                        _c("i", {
                          staticClass: "fas fa-check-circle text-success"
                        })
                      ]
                    )
                  ]),
                  _vm._v(" "),
                  _c("p", [_vm._v("74,853 views")])
                ])
              ])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "item" }, [
              _c("div", { staticClass: "category-item" }, [
                _c("a", { attrs: { href: "#" } }, [
                  _c("img", {
                    staticClass: "img-fluid",
                    attrs: { src: "img/s5.png", alt: "" }
                  }),
                  _vm._v(" "),
                  _c("h6", [_vm._v("Technology Tutorials")]),
                  _vm._v(" "),
                  _c("p", [_vm._v("74,853 views")])
                ])
              ])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "item" }, [
              _c("div", { staticClass: "category-item" }, [
                _c("a", { attrs: { href: "#" } }, [
                  _c("img", {
                    staticClass: "img-fluid",
                    attrs: { src: "img/s6.png", alt: "" }
                  }),
                  _vm._v(" "),
                  _c("h6", [_vm._v("Singing")]),
                  _vm._v(" "),
                  _c("p", [_vm._v("74,853 views")])
                ])
              ])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "item" }, [
              _c("div", { staticClass: "category-item" }, [
                _c("a", { attrs: { href: "#" } }, [
                  _c("img", {
                    staticClass: "img-fluid",
                    attrs: { src: "img/s7.png", alt: "" }
                  }),
                  _vm._v(" "),
                  _c("h6", [_vm._v("Cooking")]),
                  _vm._v(" "),
                  _c("p", [_vm._v("74,853 views")])
                ])
              ])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "item" }, [
              _c("div", { staticClass: "category-item" }, [
                _c("a", { attrs: { href: "#" } }, [
                  _c("img", {
                    staticClass: "img-fluid",
                    attrs: { src: "img/s8.png", alt: "" }
                  }),
                  _vm._v(" "),
                  _c("h6", [_vm._v("Traveling")]),
                  _vm._v(" "),
                  _c("p", [_vm._v("74,853 views")])
                ])
              ])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "item" }, [
              _c("div", { staticClass: "category-item" }, [
                _c("a", { attrs: { href: "#" } }, [
                  _c("img", {
                    staticClass: "img-fluid",
                    attrs: { src: "img/s1.png", alt: "" }
                  }),
                  _vm._v(" "),
                  _c("h6", [_vm._v("Education")]),
                  _vm._v(" "),
                  _c("p", [_vm._v("74,853 views")])
                ])
              ])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "item" }, [
              _c("div", { staticClass: "category-item" }, [
                _c("a", { attrs: { href: "#" } }, [
                  _c("img", {
                    staticClass: "img-fluid",
                    attrs: { src: "img/s2.png", alt: "" }
                  }),
                  _vm._v(" "),
                  _c("h6", [_vm._v("Noodles, Sauces & Instant Food")]),
                  _vm._v(" "),
                  _c("p", [_vm._v("74,853 views")])
                ])
              ])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "item" }, [
              _c("div", { staticClass: "category-item" }, [
                _c("a", { attrs: { href: "#" } }, [
                  _c("img", {
                    staticClass: "img-fluid",
                    attrs: { src: "img/s3.png", alt: "" }
                  }),
                  _vm._v(" "),
                  _c("h6", [
                    _vm._v("Comedy "),
                    _c(
                      "span",
                      {
                        attrs: {
                          title: "",
                          "data-placement": "top",
                          "data-toggle": "tooltip",
                          "data-original-title": "Verified"
                        }
                      },
                      [
                        _c("i", {
                          staticClass: "fas fa-check-circle text-success"
                        })
                      ]
                    )
                  ]),
                  _vm._v(" "),
                  _c("p", [_vm._v("74,853 views")])
                ])
              ])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "item" }, [
              _c("div", { staticClass: "category-item" }, [
                _c("a", { attrs: { href: "#" } }, [
                  _c("img", {
                    staticClass: "img-fluid",
                    attrs: { src: "img/s4.png", alt: "" }
                  }),
                  _vm._v(" "),
                  _c("h6", [_vm._v("Lifestyle Advice")]),
                  _vm._v(" "),
                  _c("p", [_vm._v("74,853 views")])
                ])
              ])
            ])
          ])
        ])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "col-md-12" }, [
      _c("div", { staticClass: "main-title" }, [
        _c("div", { staticClass: "btn-group float-right right-action" }, [
          _c(
            "a",
            {
              staticClass: "right-action-link text-gray",
              attrs: {
                href: "#",
                "data-toggle": "dropdown",
                "aria-haspopup": "true",
                "aria-expanded": "false"
              }
            },
            [
              _vm._v("\n                            Sort by "),
              _c("i", {
                staticClass: "fa fa-caret-down",
                attrs: { "aria-hidden": "true" }
              })
            ]
          ),
          _vm._v(" "),
          _c("div", { staticClass: "dropdown-menu dropdown-menu-right" }, [
            _c("a", { staticClass: "dropdown-item", attrs: { href: "#" } }, [
              _c("i", { staticClass: "fas fa-fw fa-star" }),
              _vm._v("   Top Rated")
            ]),
            _vm._v(" "),
            _c("a", { staticClass: "dropdown-item", attrs: { href: "#" } }, [
              _c("i", { staticClass: "fas fa-fw fa-signal" }),
              _vm._v("   Viewed")
            ]),
            _vm._v(" "),
            _c("a", { staticClass: "dropdown-item", attrs: { href: "#" } }, [
              _c("i", { staticClass: "fas fa-fw fa-times-circle" }),
              _vm._v("  \n                                Close")
            ])
          ])
        ]),
        _vm._v(" "),
        _c("h6", [_vm._v("Featured Videos")])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("a", { attrs: { href: "#" } }, [
      _c("img", {
        staticClass: "img-fluid",
        attrs: { src: "img/v1.png", alt: "" }
      })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "video-card-body" }, [
      _c("div", { staticClass: "video-title" }, [
        _c("a", { attrs: { href: "#" } }, [
          _vm._v("There are many variations of passages of Lorem")
        ])
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "video-page text-success" }, [
        _vm._v("\n                            Education "),
        _c(
          "a",
          {
            attrs: {
              title: "",
              "data-placement": "top",
              "data-toggle": "tooltip",
              href: "#",
              "data-original-title": "Verified"
            }
          },
          [_c("i", { staticClass: "fas fa-check-circle text-success" })]
        )
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "video-view" }, [
        _vm._v("\n                            1.8M views  "),
        _c("i", { staticClass: "fas fa-calendar-alt" }),
        _vm._v(" 11 Months ago\n                        ")
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("a", { attrs: { href: "#" } }, [
      _c("img", {
        staticClass: "img-fluid",
        attrs: { src: "img/v2.png", alt: "" }
      })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "video-card-body" }, [
      _c("div", { staticClass: "video-title" }, [
        _c("a", { attrs: { href: "#" } }, [
          _vm._v("There are many variations of passages of Lorem")
        ])
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "video-page text-success" }, [
        _vm._v("\n                            Education "),
        _c(
          "a",
          {
            attrs: {
              title: "",
              "data-placement": "top",
              "data-toggle": "tooltip",
              href: "#",
              "data-original-title": "Verified"
            }
          },
          [_c("i", { staticClass: "fas fa-check-circle text-success" })]
        )
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "video-view" }, [
        _vm._v("\n                            1.8M views  "),
        _c("i", { staticClass: "fas fa-calendar-alt" }),
        _vm._v(" 11 Months ago\n                        ")
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "col-xl-3 col-sm-6 mb-3" }, [
      _c("div", { staticClass: "video-card" }, [
        _c("div", { staticClass: "video-card-image" }, [
          _c("a", { staticClass: "play-icon", attrs: { href: "#" } }, [
            _c("i", { staticClass: "fas fa-play-circle" })
          ]),
          _vm._v(" "),
          _c("a", { attrs: { href: "#" } }, [
            _c("img", {
              staticClass: "img-fluid",
              attrs: { src: "img/v3.png", alt: "" }
            })
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "time" }, [_vm._v("3:50")])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "video-card-body" }, [
          _c("div", { staticClass: "video-title" }, [
            _c("a", { attrs: { href: "#" } }, [
              _vm._v("There are many variations of passages of Lorem")
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "video-page text-danger" }, [
            _vm._v("\n                            Education "),
            _c(
              "a",
              {
                attrs: {
                  title: "",
                  "data-placement": "top",
                  "data-toggle": "tooltip",
                  href: "#",
                  "data-original-title": "Unverified"
                }
              },
              [_c("i", { staticClass: "fas fa-frown text-danger" })]
            )
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "video-view" }, [
            _vm._v("\n                            1.8M views  "),
            _c("i", { staticClass: "fas fa-calendar-alt" }),
            _vm._v(" 11 Months ago\n                        ")
          ])
        ])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "col-xl-3 col-sm-6 mb-3" }, [
      _c("div", { staticClass: "video-card" }, [
        _c("div", { staticClass: "video-card-image" }, [
          _c("a", { staticClass: "play-icon", attrs: { href: "#" } }, [
            _c("i", { staticClass: "fas fa-play-circle" })
          ]),
          _vm._v(" "),
          _c("a", { attrs: { href: "#" } }, [
            _c("img", {
              staticClass: "img-fluid",
              attrs: { src: "img/v4.png", alt: "" }
            })
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "time" }, [_vm._v("3:50")])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "video-card-body" }, [
          _c("div", { staticClass: "video-title" }, [
            _c("a", { attrs: { href: "#" } }, [
              _vm._v("There are many variations of passages of Lorem")
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "video-page text-success" }, [
            _vm._v("\n                            Education "),
            _c(
              "a",
              {
                attrs: {
                  title: "",
                  "data-placement": "top",
                  "data-toggle": "tooltip",
                  href: "#",
                  "data-original-title": "Verified"
                }
              },
              [_c("i", { staticClass: "fas fa-check-circle text-success" })]
            )
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "video-view" }, [
            _vm._v("\n                            1.8M views  "),
            _c("i", { staticClass: "fas fa-calendar-alt" }),
            _vm._v(" 11 Months ago\n                        ")
          ])
        ])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "col-xl-3 col-sm-6 mb-3" }, [
      _c("div", { staticClass: "video-card" }, [
        _c("div", { staticClass: "video-card-image" }, [
          _c("a", { staticClass: "play-icon", attrs: { href: "#" } }, [
            _c("i", { staticClass: "fas fa-play-circle" })
          ]),
          _vm._v(" "),
          _c("a", { attrs: { href: "#" } }, [
            _c("img", {
              staticClass: "img-fluid",
              attrs: { src: "img/v5.png", alt: "" }
            })
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "time" }, [_vm._v("3:50")])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "video-card-body" }, [
          _c("div", { staticClass: "video-title" }, [
            _c("a", { attrs: { href: "#" } }, [
              _vm._v("There are many variations of passages of Lorem")
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "video-page text-success" }, [
            _vm._v("\n                            Education "),
            _c(
              "a",
              {
                attrs: {
                  title: "",
                  "data-placement": "top",
                  "data-toggle": "tooltip",
                  href: "#",
                  "data-original-title": "Verified"
                }
              },
              [_c("i", { staticClass: "fas fa-check-circle text-success" })]
            )
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "video-view" }, [
            _vm._v("\n                            1.8M views  "),
            _c("i", { staticClass: "fas fa-calendar-alt" }),
            _vm._v(" 11 Months ago\n                        ")
          ])
        ])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "col-xl-3 col-sm-6 mb-3" }, [
      _c("div", { staticClass: "video-card" }, [
        _c("div", { staticClass: "video-card-image" }, [
          _c("a", { staticClass: "play-icon", attrs: { href: "#" } }, [
            _c("i", { staticClass: "fas fa-play-circle" })
          ]),
          _vm._v(" "),
          _c("a", { attrs: { href: "#" } }, [
            _c("img", {
              staticClass: "img-fluid",
              attrs: { src: "img/v6.png", alt: "" }
            })
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "time" }, [_vm._v("3:50")])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "video-card-body" }, [
          _c("div", { staticClass: "video-title" }, [
            _c("a", { attrs: { href: "#" } }, [
              _vm._v("There are many variations of passages of Lorem")
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "video-page text-danger" }, [
            _vm._v("\n                            Education "),
            _c(
              "a",
              {
                attrs: {
                  title: "",
                  "data-placement": "top",
                  "data-toggle": "tooltip",
                  href: "#",
                  "data-original-title": "Unverified"
                }
              },
              [_c("i", { staticClass: "fas fa-frown text-danger" })]
            )
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "video-view" }, [
            _vm._v("\n                            1.8M views  "),
            _c("i", { staticClass: "fas fa-calendar-alt" }),
            _vm._v(" 11 Months ago\n                        ")
          ])
        ])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "col-xl-3 col-sm-6 mb-3" }, [
      _c("div", { staticClass: "video-card" }, [
        _c("div", { staticClass: "video-card-image" }, [
          _c("a", { staticClass: "play-icon", attrs: { href: "#" } }, [
            _c("i", { staticClass: "fas fa-play-circle" })
          ]),
          _vm._v(" "),
          _c("a", { attrs: { href: "#" } }, [
            _c("img", {
              staticClass: "img-fluid",
              attrs: { src: "img/v7.png", alt: "" }
            })
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "time" }, [_vm._v("3:50")])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "video-card-body" }, [
          _c("div", { staticClass: "video-title" }, [
            _c("a", { attrs: { href: "#" } }, [
              _vm._v("There are many variations of passages of Lorem")
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "video-page text-success" }, [
            _vm._v("\n                            Education "),
            _c(
              "a",
              {
                attrs: {
                  title: "",
                  "data-placement": "top",
                  "data-toggle": "tooltip",
                  href: "#",
                  "data-original-title": "Verified"
                }
              },
              [_c("i", { staticClass: "fas fa-check-circle text-success" })]
            )
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "video-view" }, [
            _vm._v("\n                            1.8M views  "),
            _c("i", { staticClass: "fas fa-calendar-alt" }),
            _vm._v(" 11 Months ago\n                        ")
          ])
        ])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "col-xl-3 col-sm-6 mb-3" }, [
      _c("div", { staticClass: "video-card" }, [
        _c("div", { staticClass: "video-card-image" }, [
          _c("a", { staticClass: "play-icon", attrs: { href: "#" } }, [
            _c("i", { staticClass: "fas fa-play-circle" })
          ]),
          _vm._v(" "),
          _c("a", { attrs: { href: "#" } }, [
            _c("img", {
              staticClass: "img-fluid",
              attrs: { src: "img/v8.png", alt: "" }
            })
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "time" }, [_vm._v("3:50")])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "video-card-body" }, [
          _c("div", { staticClass: "video-title" }, [
            _c("a", { attrs: { href: "#" } }, [
              _vm._v("There are many variations of passages of Lorem")
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "video-page text-success" }, [
            _vm._v("\n                            Education "),
            _c(
              "a",
              {
                attrs: {
                  title: "",
                  "data-placement": "top",
                  "data-toggle": "tooltip",
                  href: "#",
                  "data-original-title": "Verified"
                }
              },
              [_c("i", { staticClass: "fas fa-check-circle text-success" })]
            )
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "video-view" }, [
            _vm._v("\n                            1.8M views  "),
            _c("i", { staticClass: "fas fa-calendar-alt" }),
            _vm._v(" 11 Months ago\n                        ")
          ])
        ])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "video-block section-padding" }, [
      _c("div", { staticClass: "row" }, [
        _c("div", { staticClass: "col-md-12" }, [
          _c("div", { staticClass: "main-title" }, [
            _c("div", { staticClass: "btn-group float-right right-action" }, [
              _c(
                "a",
                {
                  staticClass: "right-action-link text-gray",
                  attrs: {
                    href: "#",
                    "data-toggle": "dropdown",
                    "aria-haspopup": "true",
                    "aria-expanded": "false"
                  }
                },
                [
                  _vm._v("\n                            Sort by "),
                  _c("i", {
                    staticClass: "fa fa-caret-down",
                    attrs: { "aria-hidden": "true" }
                  })
                ]
              ),
              _vm._v(" "),
              _c("div", { staticClass: "dropdown-menu dropdown-menu-right" }, [
                _c(
                  "a",
                  { staticClass: "dropdown-item", attrs: { href: "#" } },
                  [
                    _c("i", { staticClass: "fas fa-fw fa-star" }),
                    _vm._v("   Top Rated")
                  ]
                ),
                _vm._v(" "),
                _c(
                  "a",
                  { staticClass: "dropdown-item", attrs: { href: "#" } },
                  [
                    _c("i", { staticClass: "fas fa-fw fa-signal" }),
                    _vm._v("   Viewed")
                  ]
                ),
                _vm._v(" "),
                _c(
                  "a",
                  { staticClass: "dropdown-item", attrs: { href: "#" } },
                  [
                    _c("i", { staticClass: "fas fa-fw fa-times-circle" }),
                    _vm._v("  \n                                Close")
                  ]
                )
              ])
            ]),
            _vm._v(" "),
            _c("h6", [_vm._v("Popular Channels")])
          ])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "col-xl-3 col-sm-6 mb-3" }, [
          _c("div", { staticClass: "channels-card" }, [
            _c("div", { staticClass: "channels-card-image" }, [
              _c("a", { attrs: { href: "#" } }, [
                _c("img", {
                  staticClass: "img-fluid",
                  attrs: { src: "img/s1.png", alt: "" }
                })
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "channels-card-image-btn" }, [
                _c(
                  "button",
                  {
                    staticClass: "btn btn-outline-danger btn-sm",
                    attrs: { type: "button" }
                  },
                  [
                    _vm._v("Subscribe\n                                "),
                    _c("strong", [_vm._v("1.4M")])
                  ]
                )
              ])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "channels-card-body" }, [
              _c("div", { staticClass: "channels-title" }, [
                _c("a", { attrs: { href: "#" } }, [_vm._v("Channels Name")])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "channels-view" }, [
                _vm._v(
                  "\n                            382,323 subscribers\n                        "
                )
              ])
            ])
          ])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "col-xl-3 col-sm-6 mb-3" }, [
          _c("div", { staticClass: "channels-card" }, [
            _c("div", { staticClass: "channels-card-image" }, [
              _c("a", { attrs: { href: "#" } }, [
                _c("img", {
                  staticClass: "img-fluid",
                  attrs: { src: "img/s2.png", alt: "" }
                })
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "channels-card-image-btn" }, [
                _c(
                  "button",
                  {
                    staticClass: "btn btn-outline-danger btn-sm",
                    attrs: { type: "button" }
                  },
                  [
                    _vm._v("Subscribe\n                                "),
                    _c("strong", [_vm._v("1.4M")])
                  ]
                )
              ])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "channels-card-body" }, [
              _c("div", { staticClass: "channels-title" }, [
                _c("a", { attrs: { href: "#" } }, [_vm._v("Channels Name")])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "channels-view" }, [
                _vm._v(
                  "\n                            382,323 subscribers\n                        "
                )
              ])
            ])
          ])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "col-xl-3 col-sm-6 mb-3" }, [
          _c("div", { staticClass: "channels-card" }, [
            _c("div", { staticClass: "channels-card-image" }, [
              _c("a", { attrs: { href: "#" } }, [
                _c("img", {
                  staticClass: "img-fluid",
                  attrs: { src: "img/s3.png", alt: "" }
                })
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "channels-card-image-btn" }, [
                _c(
                  "button",
                  {
                    staticClass: "btn btn-outline-secondary btn-sm",
                    attrs: { type: "button" }
                  },
                  [
                    _vm._v("Subscribed\n                                "),
                    _c("strong", [_vm._v("1.4M")])
                  ]
                )
              ])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "channels-card-body" }, [
              _c("div", { staticClass: "channels-title" }, [
                _c("a", { attrs: { href: "#" } }, [
                  _vm._v("Channels Name "),
                  _c(
                    "span",
                    {
                      attrs: {
                        title: "",
                        "data-placement": "top",
                        "data-toggle": "tooltip",
                        "data-original-title": "Verified"
                      }
                    },
                    [_c("i", { staticClass: "fas fa-check-circle" })]
                  )
                ])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "channels-view" }, [
                _vm._v(
                  "\n                            382,323 subscribers\n                        "
                )
              ])
            ])
          ])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "col-xl-3 col-sm-6 mb-3" }, [
          _c("div", { staticClass: "channels-card" }, [
            _c("div", { staticClass: "channels-card-image" }, [
              _c("a", { attrs: { href: "#" } }, [
                _c("img", {
                  staticClass: "img-fluid",
                  attrs: { src: "img/s4.png", alt: "" }
                })
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "channels-card-image-btn" }, [
                _c(
                  "button",
                  {
                    staticClass: "btn btn-outline-danger btn-sm",
                    attrs: { type: "button" }
                  },
                  [
                    _vm._v("Subscribe\n                                "),
                    _c("strong", [_vm._v("1.4M")])
                  ]
                )
              ])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "channels-card-body" }, [
              _c("div", { staticClass: "channels-title" }, [
                _c("a", { attrs: { href: "#" } }, [_vm._v("Channels Name")])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "channels-view" }, [
                _vm._v(
                  "\n                            382,323 subscribers\n                        "
                )
              ])
            ])
          ])
        ])
      ])
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-440dff1c", module.exports)
  }
}

/***/ }),

/***/ 190:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(191)
}
var normalizeComponent = __webpack_require__(0)
/* script */
var __vue_script__ = __webpack_require__(193)
/* template */
var __vue_template__ = __webpack_require__(194)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/components/VideoPlayer.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-52ca88d5", Component.options)
  } else {
    hotAPI.reload("data-v-52ca88d5", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 191:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(192);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(3)("7ff51343", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-52ca88d5\",\"scoped\":false,\"hasInlineConfig\":true}!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./VideoPlayer.vue", function() {
     var newContent = require("!!../../../../node_modules/css-loader/index.js!../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-52ca88d5\",\"scoped\":false,\"hasInlineConfig\":true}!../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./VideoPlayer.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 192:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(2)(false);
// imports


// module
exports.push([module.i, "\n.single-video.landing {\n    width: 100% !important;\n}\n\n/*.single-video*/\n", ""]);

// exports


/***/ }),

/***/ 193:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
    name: 'BigVideoPlayer',
    data: function data() {
        return {};
    }
});

/***/ }),

/***/ 194:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "video-block section-padding" }, [
    _c("div", { staticClass: "row" }, [
      _c("div", { staticClass: "col-md-12" }, [
        _c("div", { staticClass: "single-video-left" }, [
          _vm._m(0),
          _vm._v(" "),
          _c("div", { staticClass: "single-video-title box mb-3" }, [
            _c(
              "h2",
              [
                _c(
                  "router-link",
                  {
                    attrs: {
                      to: { name: "video", params: { id: 1 } },
                      href: "#"
                    }
                  },
                  [
                    _vm._v(
                      "\n                            Welcome to WebbiWood\n                        "
                    )
                  ]
                )
              ],
              1
            ),
            _vm._v(" "),
            _vm._m(1)
          ])
        ])
      ])
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "single-video landing" }, [
      _c("video", {
        staticStyle: { width: "inherit !important" },
        attrs: {
          width: "100%",
          height: "300px",
          autoplay: "",
          src: "http://webbiwood.com/webbiwood.mp4",
          controls: ""
        }
      })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("p", { staticClass: "mb-0" }, [
      _c("i", { staticClass: "fas fa-eye" }),
      _vm._v(" 2,729,347 views")
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-52ca88d5", module.exports)
  }
}

/***/ })

});